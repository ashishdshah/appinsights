﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AiDataChannel.Models;

namespace AiDataChannel.Tests.EndToEnd
{
    public class Book
    {
        private int _counter;
        private string[] _bookTitles = { "Lord Of The Rings", "Game Of Thrones", "Prisoner Of Azkaban" };
        private string[] _sessionIds = { Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString() };
        private string[] _clientDips = { "10.12.34.11", "10.56.22.7" };
        Random _rnd = new Random();
        private List<Event> _events = new List<Event>();

        public Book(int i) {
            _counter = i;
        }

        public List<Event> Events { get { return _events; } }

        public Event GenerateEvent(Metric[] metrics) {
            Event bookEvent = new Event {
                clientDip = _clientDips[_rnd.Next(_clientDips.Length)],
                componentName = "Book.DowWork",
                eventType = "sales",
                message = "",
                metrics = metrics,
                sessionId = _sessionIds[_rnd.Next(_sessionIds.Length)],
                severity = "",
                timeStamp = DateTime.Now.ToUniversalTime()
            };
            return bookEvent;
        }

        public void DoWork() {
            Metric pages = new Metric { name = "pages", value = _rnd.Next(100) };
            Metric copiesSold = new Metric { name = "copiesSold", value = _rnd.Next(1000) };
            Metric title = new Metric {
                name = "title",
                value = _bookTitles[_rnd.Next(_bookTitles.Length)]
            };
            Event workEvent = GenerateEvent(new Metric[] { pages, copiesSold, title });
            _events.Add(workEvent);

            SummarizeBook();
            PrintSummary();
            if (_counter % 2 == 0)
                PrintRawSummaryRatio();
        }

        private void PrintSummary() {
            Metric printed = new Metric { name = "printed", value = 1 };
            Event summaryEvent = GenerateEvent(new Metric[] { printed });
            _events.Add(summaryEvent);

            if (_counter % 6 == 0) {
                try {
                    NeedAnError();
                }
                catch (Exception ex) {
                    Event errorEvent = GenerateEvent(new Metric[0]);
                    errorEvent.severity = "error";
                    errorEvent.message = string.Format("{0}\n{1}", ex.Message, ex.StackTrace);
                    _events.Add(errorEvent);
                }
            }
        }

        private void NeedAnError() {
            try {
                NeedAnError2Deep();
            }
            catch (Exception ex) {
                throw new Exception("From 1 Deep", ex);
            }
        }

        private void NeedAnError2Deep() {
            throw new Exception("From 2 Deep");
        }

        private void PrintRawSummaryRatio() {
            Metric rsm = new Metric { name = "ratio", value = _rnd.NextDouble() };
            Event rse = GenerateEvent(new Metric[] { rsm });
            _events.Add(rse);
        }

        private void SummarizeBook() {
            Metric sm = new Metric { name = "summary", value = true };
            Event se = GenerateEvent(new Metric[] { sm });
            _events.Add(se);
        }
    }
}
