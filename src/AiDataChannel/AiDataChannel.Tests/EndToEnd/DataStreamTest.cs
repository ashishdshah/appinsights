﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AiDataChannel.Models;
using log4net;
using log4net.Config;
using System.Net;
using System.IO;
using AiDataChannel.Helpers;

namespace AiDataChannel.Tests.EndToEnd
{
    [TestClass]
    public class DataStreamTest
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string _url = "http://localhost:51420/DataStream";
        //private string _url = "http://localhost/api/DataStream";
        //private string _url = "http://107.22.212.75/api/DataStream";

        public DataStreamTest() {
            XmlConfigurator.Configure();
        }

        [TestMethod]
        public void TestSimplePost() {
            Batch batch = GenerateTestEvents("a9654705a992c4c528452a3ae00b89f99");
            string jsonEvents = BatchToJson(batch);
            HttpWebResponse webResponse = PostEventStream(_url, jsonEvents);
            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.Accepted, webResponse.StatusCode);
            // TODO: Check that the right entries are there in the db.

            batch = GenerateTestEvents("wrong app key");
            jsonEvents = BatchToJson(batch);
            webResponse = PostEventStream(_url, jsonEvents);
            // The service should simply accept the value but the db should not have any entries
            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.Accepted, webResponse.StatusCode);
            // TODO: Check that the db does not have any entries

            // TODO: Dont know what the following 2 lines are for - probably to be deleted.
            //webResponse = PostEventStream(_url + "/TestSync", jsonEvents);
            //Assert.AreEqual<HttpStatusCode>(HttpStatusCode.Accepted, webResponse.StatusCode);
        }

        [TestMethod]
        public void TestEmptyPost() {
            string eventStream = "{\"applicationKey\":\"javascript_test\",\"events\":[{\"_id\":0,\"componentName\":\"Book.DowWork\",\"eventType\":\"sales\",\"metrics\":[],\"timeStamp\":\"\\/Date(1338102000000)\\/\"},{\"clientDip\":\"10.1.1.1\",\"componentName\":\"Book.DowWork\",\"eventType\":\"sales\",\"message\":\"\",\"metrics\":[{\"name\":\"pages\",\"value\":20},{\"name\":\"copiesSold\",\"value\":200},{\"name\":\"title\",\"value\":\"Lord Of The Rings\"}],\"sessionId\":\"b3a34a02-ec2d-4161-8555-b7f653f00f0f\",\"severity\":\"\",\"timeStamp\":\"\\/Date(1338102000000)\\/\"}],\"numberOfEvents\":2}";

            HttpWebResponse webResponse = PostEventStream(_url, eventStream);
            Assert.AreEqual<HttpStatusCode>(HttpStatusCode.Accepted, webResponse.StatusCode);            
        }

        private HttpWebResponse PostEventStream(string url, string jsonEvents) {
            DateTime start = DateTime.Now;

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            byte[] bytedata = Encoding.UTF8.GetBytes("eventStream=" + jsonEvents);
            webRequest.ContentLength = bytedata.Length;

            Stream requestStream = webRequest.GetRequestStream();
            requestStream.Write(bytedata, 0, bytedata.Length);
            requestStream.Close();

            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();            

            DateTime end = DateTime.Now;
            TimeSpan perf = end - start;
            logger.Debug(string.Format("Time taken for round trip to {0}: {1}", url, perf.ToString()));
            return webResponse;
        }

        private Batch GenerateTestEvents(string appKey) {
            List<Event> eventList = new List<Event>();
            for (int i = 0; i < 3000; i++) {
                Book book = new Book(i);
                book.DoWork();
                eventList.AddRange(book.Events);
            }
            return new Batch {
                applicationKey = appKey, 
                events = eventList.ToArray(), 
                numberOfEvents = eventList.Count 
            };            
        }

        private string BatchToJson(Batch batch) {
            string jsonEvents = JsonConverter.Serialize<Batch>(batch);
            return jsonEvents;
        }
    }
}
