﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AiDataChannel.Models;
using AiDataChannel.Helpers;
using log4net;
using log4net.Config;

namespace AiDataChannel.Tests.Models
{
    /*
    {
	applicationKey:"appkey here",
	events:[
		{
			_id:{ _increment:0, _machine:0, _pid:0, _timestamp:0 },
			clientDip:"10.1.1.1",
			componentName:"Book.DowWork",
			eventType:"sales",
			message:"",
			metrics:[
				{name:"pages",value:20},
				{name:"copiesSold",value:200},
				{name:"title",value:"Lord Of The Rings"}
			],
			sessionId:"b3a34a02-ec2d-4161-8555-b7f653f00f0f",
			severity:"",
			timeStamp:"\/Date(1338102000000)\/"
		},
		{
			_id:{ _increment:0, _machine:0, _pid:0, _timestamp:0 },
			clientDip:"10.1.1.2",
			componentName:"Book.DowWork",
			eventType:"sales",
			message:"",
			metrics:[
				{name:"pages",value:30},
				{name:"copiesSold",value:100},
				{name:"title",value:"Game Of Thrones"}
			],
			sessionId:"b3a34a02-ec2d-4161-8555-b7f653f00f0f",
			severity:"",
			timeStamp:"\/Date(1338102000000)\/"
		}
	],
	numberOfEvents:2
}
     */
    [TestClass]
    public class EventTest
    {
        private Event event1;
        private string jsonEvent1;

        private Event event2;
                
        private Event[] events;
        private string jsonEvents;

        private Batch batch;
        private string jsonBatch;

        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public EventTest() {
            XmlConfigurator.Configure();

            Metric pages = new Metric { name = "pages", value = 20 };
            Metric copiesSold = new Metric { name = "copiesSold", value = 200 };
            Metric title = new Metric {
                name = "title",
                value = "Lord Of The Rings"
            };
            event1 = new Event {
                clientDip = "10.1.1.1",
                componentName = "Book.DowWork",
                eventType = "sales",
                message = "",
                metrics = new Metric[] { pages, copiesSold, title },
                sessionId = "b3a34a02-ec2d-4161-8555-b7f653f00f0f",
                severity = "",
                timeStamp = DateTime.Parse("5/27/2012").ToUniversalTime()
            };
            jsonEvent1 = "{\"_id\":{\"_increment\":0,\"_machine\":0,\"_pid\":0,\"_timestamp\":0},\"clientDip\":\"10.1.1.1\",\"componentName\":\"Book.DowWork\",\"eventType\":\"sales\",\"message\":\"\",\"metrics\":[{\"name\":\"pages\",\"value\":20},{\"name\":\"copiesSold\",\"value\":200},{\"name\":\"title\",\"value\":\"Lord Of The Rings\"}],\"sessionId\":\"b3a34a02-ec2d-4161-8555-b7f653f00f0f\",\"severity\":\"\",\"timeStamp\":\"\\/Date(1338102000000)\\/\"}";

            Metric pages2 = new Metric { name = "pages", value = 30 };
            Metric copiesSold2 = new Metric { name = "copiesSold", value = 100 };
            Metric title2 = new Metric {
                name = "title",
                value = "Game Of Thrones"
            };
            event2 = new Event {
                clientDip = "10.1.1.2",
                componentName = "Book.DowWork",
                eventType = "sales",
                message = "",
                metrics = new Metric[] { pages2, copiesSold2, title2 },
                sessionId = "b3a34a02-ec2d-4161-8555-b7f653f00f0f",
                severity = "",
                timeStamp = DateTime.Parse("5/27/2012").ToUniversalTime()
            };

            events = new Event[] { event1, event2 };
            jsonEvents = "[{\"_id\":{\"_increment\":0,\"_machine\":0,\"_pid\":0,\"_timestamp\":0},\"clientDip\":\"10.1.1.1\",\"componentName\":\"Book.DowWork\",\"eventType\":\"sales\",\"message\":\"\",\"metrics\":[{\"name\":\"pages\",\"value\":20},{\"name\":\"copiesSold\",\"value\":200},{\"name\":\"title\",\"value\":\"Lord Of The Rings\"}],\"sessionId\":\"b3a34a02-ec2d-4161-8555-b7f653f00f0f\",\"severity\":\"\",\"timeStamp\":\"\\/Date(1338102000000)\\/\"},{\"_id\":{\"_increment\":0,\"_machine\":0,\"_pid\":0,\"_timestamp\":0},\"clientDip\":\"10.1.1.2\",\"componentName\":\"Book.DowWork\",\"eventType\":\"sales\",\"message\":\"\",\"metrics\":[{\"name\":\"pages\",\"value\":30},{\"name\":\"copiesSold\",\"value\":100},{\"name\":\"title\",\"value\":\"Game Of Thrones\"}],\"sessionId\":\"b3a34a02-ec2d-4161-8555-b7f653f00f0f\",\"severity\":\"\",\"timeStamp\":\"\\/Date(1338102000000)\\/\"}]";

            batch = new Batch { 
                applicationKey = "appkey here", 
                numberOfEvents = 2, 
                events = new Event[] { event1, event2 } 
            };
            jsonBatch = "{\"applicationKey\":\"appkey here\",\"events\":" + jsonEvents + ",\"numberOfEvents\":2}";
        }

        [TestMethod]
        public void TestPartialSerialization() {
            string evntJson = "{\"componentName\":\"Book.DowWork\",\"eventType\":\"sales\",\"timeStamp\":\"\\/Date(1338102000000)\\/\"}";
            Event evnt = Event.CreateFromJson(evntJson);
            //logger.Info(evnt.ToString());

            string batchJson = "{\"applicationKey\":\"javascript_test\",\"events\":[{\"componentName\":\"Book.DowWork\",\"eventType\":\"sales\",\"timeStamp\":\"\\/Date(1338102000000)\\/\"},{\"clientDip\":\"10.1.1.1\",\"componentName\":\"Book.DowWork\",\"eventType\":\"sales\",\"message\":\"\",\"metrics\":[{\"name\":\"pages\",\"value\":20},{\"name\":\"copiesSold\",\"value\":200},{\"name\":\"title\",\"value\":\"Lord Of The Rings\"}],\"sessionId\":\"b3a34a02-ec2d-4161-8555-b7f653f00f0f\",\"severity\":\"\",\"timeStamp\":\"\\/Date(1338102000000)\\/\"}],\"numberOfEvents\":2}";
            Batch batch = Batch.CreateFromJson(batchJson);
            logger.Debug(batch);
        }

        [TestMethod]
        public void TestSerialization() {
            string obsJsonEvent1 = event1.ToString();
            Assert.AreEqual<string>(jsonEvent1, obsJsonEvent1);

            Event obsEvent1 = Event.CreateFromJson(jsonEvent1);
            Assert.AreEqual<Event>(event1, obsEvent1);

            Event[] obsEvents = Event.CreateArrayFromJson(jsonEvents);
            CollectionAssert.AreEquivalent(events, obsEvents);

            string obsJsonBatch = batch.ToString();
            Assert.AreEqual<string>(jsonBatch, obsJsonBatch);

            Batch obsBatch = Batch.CreateFromJson(jsonBatch);
            Assert.AreEqual<Batch>(batch, obsBatch);

            logger.Info(event1.ToString());
            logger.Info(batch.ToString());
        }
    }
}
