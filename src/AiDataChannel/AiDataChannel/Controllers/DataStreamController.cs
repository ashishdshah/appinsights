﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using AiDataChannel.Models;
using System.Net;
using AiDataChannel.Helpers;
using System.Text;

namespace AiDataChannel.Controllers
{    
    public class DataStreamController : Controller
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpPost]
        public ActionResult ValidApplicationKey(string applicationKey) {
            DataLayer.SaveApplicationKey(applicationKey);
            return new EmptyResult();
        }

        [HttpPost]
        [AllowCrossSite]
        public ActionResult Index(string eventStream) {
            if (eventStream == null) {
                int totalReadBytes = 0;
                byte[] contentBuffer = new byte[Request.ContentLength];
                while (totalReadBytes < Request.ContentLength) {
                    byte[] buffer = new byte[Request.ContentLength];
                    int readBytes = Request.InputStream.Read(buffer, 0, Request.ContentLength);
                    for (int i = totalReadBytes; i < readBytes + totalReadBytes; i++) {
                        contentBuffer[i] = buffer[i - totalReadBytes];
                    }
                    totalReadBytes += readBytes;
                }
                // Assume content type to UTF-8
                Encoding enc = Encoding.UTF8;
                string content = enc.GetString(contentBuffer);
                if (content.StartsWith("eventStream=")) {
                    eventStream = content.Substring("eventStream=".Length);
                }                
            }

            if (!string.IsNullOrEmpty(eventStream)) {
                Batch batch = Batch.CreateFromJson(eventStream);
                if (batch.numberOfEvents > 0) {
                    DataLayer.AsyncSave(batch);
                }
                Response.StatusCode = (int)HttpStatusCode.Accepted;                
            }
            else {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            return new EmptyResult();
        }        
    }
}
