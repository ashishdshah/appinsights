﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using AiDataChannel.Helpers;

namespace AiDataChannel.Controllers
{    
    public class HomeController : Controller
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public ActionResult Index() {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult About() {
            return View();
        }

        public ActionResult TestLogs() {
            logger.Debug("This is debug log");
            logger.Info("This is info log");
            return new EmptyResult();
        }

        public ActionResult TestErrorHandling() {
            throw new Exception("hahaha");
        }

        public ActionResult GenerateTestData(string appKey, int numEvents) {
            DataLayer.AsyncGenerateTestData(appKey, numEvents);
            return new EmptyResult();
        }

    }
}
