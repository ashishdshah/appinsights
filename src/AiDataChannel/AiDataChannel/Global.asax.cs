﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net.Config;
using AiDataChannel.Helpers;
using log4net;

namespace AiDataChannel
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new LogErrorsAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            XmlConfigurator.Configure();
        }

        protected void Application_Error() {
            Exception ex = Server.GetLastError();
            string source = "some source";
            string msg = string.Format("source: {0}\nmessage: {1}\nstacktrace: {2}",
                source, ex.Message, ex.StackTrace);
            logger.Fatal(msg);
            HttpContext.Current.Server.ClearError();
            HttpContext.Current.Response.Redirect("~/Views/Shared/Error.cshtml");
        }
    }
}