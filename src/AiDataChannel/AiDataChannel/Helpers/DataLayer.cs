﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using AiDataChannel.Models;
using System.Web.Configuration;
using System.Threading;
using MongoDB.Driver.Builders;

namespace AiDataChannel.Helpers
{
    public class DataLayer
    {
        private static MongoDatabase db;
        private static MongoCollection events;

        static DataLayer() {
            string connStr = WebConfigurationManager.ConnectionStrings["mongoConnStr"].ConnectionString;
            MongoServer server = MongoServer.Create(connStr);
            db = server.GetDatabase("AiEventStream");
            //events = db.GetCollection("events");
        }

        public static void SaveApplicationKey(string appKey) {
            MongoCollection appKeysColl = db.GetCollection("validApplicationKeys");
            ApplicationKeys appKeys = new ApplicationKeys { applicationKey = appKey };
            appKeysColl.Insert<ApplicationKeys>(appKeys);
        }

        public static void Save(object data) {
            Batch batch = data as Batch;
            MongoCollection appKeys = db.GetCollection("validApplicationKeys");
            var query = Query.EQ("applicationKey", batch.applicationKey);
            var entity = appKeys.FindOneAs<ApplicationKeys>(query);
            if (entity != null) {
                events = db.GetCollection(GenerateCollectionName(batch.applicationKey));
                events.InsertBatch<Event>(batch.events);
            }
            else {
                // TODO: log a warning
            }
        }

        public static void AsyncSave(Batch batch) {
            Thread save = new Thread(new ParameterizedThreadStart(DataLayer.Save));
            save.Start(batch);
        }

        private static string GenerateCollectionName(string applicationKey) {
            return applicationKey + "_events";
        }

        public static void AsyncGenerateTestData(string appKey, int numEvents) {
            Thread genTestData = new Thread(new ParameterizedThreadStart(DataLayer.GenerateTestData));
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters["appKey"] = appKey;
            parameters["numEvents"] = numEvents;
            genTestData.Start(parameters);
        }

        public static void GenerateTestData(object data) {
            Dictionary<string, object> parameters = data as Dictionary<string, object>;
            string appKey = parameters["appKey"] as string;
            int numEvents = (int)parameters["numEvents"];
            Save(TestDataGenerator.Generate(appKey, numEvents));
        }
    }
}