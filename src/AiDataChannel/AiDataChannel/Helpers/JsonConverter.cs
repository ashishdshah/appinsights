﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;

namespace AiDataChannel.Helpers
{
    public class JsonConverter
    {
        public static T Deserialise<T>(string json) {
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json))) {
                var serialiser = new DataContractJsonSerializer(typeof(T));
                return (T)serialiser.ReadObject(ms);
            }
        }

        public static string Serialize<T>(T obj) {
            var serializer = new DataContractJsonSerializer(obj.GetType());
            using (var ms = new MemoryStream()) {
                serializer.WriteObject(ms, obj);
                return Encoding.Default.GetString(ms.ToArray());
            }
        }
    }
}