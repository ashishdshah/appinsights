﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace AiDataChannel.Helpers
{
    public class LogErrorsAttribute : HandleErrorAttribute
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnException(ExceptionContext filterContext) {
            Exception ex = filterContext.Exception;
            string source = string.Format("{0}/{1}", filterContext.RouteData.Values["controller"], filterContext.RouteData.Values["action"]);
            string msg = string.Format("source: {0}\nmessage: {1}\nstacktrace: {2}", 
                source, ex.Message, ex.StackTrace);
            logger.Fatal(msg);            
            base.OnException(filterContext);
        }
    }
}