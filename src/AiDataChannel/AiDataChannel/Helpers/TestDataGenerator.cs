﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AiDataChannel.Models;

namespace AiDataChannel.Helpers
{
    public class TestDataGenerator
    {
        private static DateTime RandomDay(DateTime start, Random gen, int range)
        {
            return start.AddDays(gen.Next(range));
        }

        public static Batch Generate(string appKey, int numEvents) {
            List<string> sessionIds = new List<string> { "12345", "6789" };
            List<string> clientDips = new List<string> { "10.1.1.1", "10.2.2.2" };
            List<string> components = new List<string> { "OMS", "CMS", "Checkout", "DBLayer" };
            List<string> eventTypes = new List<string> { "checkoutpipeline", "browse", "shipping", "youraccount" };
            List<string> severities = new List<string> { "all", "error", "warn", "info", "debug" };
            List<string> metrics = new List<string> { "cpu", "qlen", "operation", "size", "geoRegion" };
            List<string> operations = new List<string> { "uploadDoc", "downloadDoc", "deleteDoc", "transferDoc" };
            List<string> geoRegions = new List<string> { "East US", "West US", "Asia", "Europe" };
            List<string> messages = new List<string> { "", "quick brown fox jumped over the dog", "lorem ipsum dolor set", "consectetur adipisicing elit ipsum set", "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ipsum" };
            DateTime start = new DateTime(2011, 9, 1); ; // DateTime.Parse("Sunday, August 26, 2012 12:30:54 AM").ToUniversalTime();

            Batch batch = new Batch {
                applicationKey = appKey, 
                numberOfEvents = numEvents, 
                events = new Event[numEvents] 
            };
            Random rnd = new Random();
            Random gen = new Random();
            int rangeTimestamp = ((TimeSpan)(DateTime.Today - start)).Days;

            for (int i = 0; i < numEvents; i++) {
                Event evnt = new Event();
                evnt.clientDip = clientDips[rnd.Next(clientDips.Count)];
                evnt.componentName = components[rnd.Next(components.Count)];
                evnt.eventType = eventTypes[rnd.Next(eventTypes.Count)];
                evnt.message = messages[rnd.Next(messages.Count)];
                evnt.sessionId = sessionIds[rnd.Next(sessionIds.Count)];
                evnt.severity = severities[rnd.Next(severities.Count)];
                evnt.timeStamp = RandomDay(start, gen, rangeTimestamp);   //start.AddSeconds(rnd.Next(86400));
                int numMetrics = rnd.Next(6);
                evnt.metrics = new Metric[numMetrics];
                for (int j = 0; j < numMetrics; j++) {
                    Metric m = new Metric();
                    m.name = metrics[rnd.Next(metrics.Count)];
                    switch (m.name) {
                        case "cpu":
                            m.value = rnd.Next(100);
                            break;
                        case "qlen":
                            m.value = rnd.Next(1000);
                            break;
                        case "operation":
                            m.value = operations[rnd.Next(operations.Count)];
                            break;
                        case "size":
                            m.value = rnd.Next(100) + rnd.NextDouble();
                            break;
                        case "geoRegion":
                            m.value = geoRegions[rnd.Next(geoRegions.Count)];
                            break;
                    }
                    evnt.metrics[j] = m;
                }
                batch.events[i] = evnt;
            }
            return batch;
        }
    }
}