﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using AiDataChannel.Helpers;

namespace AiDataChannel.Models
{
    public class ApplicationKeys
    {
        public ObjectId _id { get; set; }
        public string applicationKey { get; set; }

        #region plumbing
        public static ApplicationKeys CreateFromJson(string json) {
            return JsonConverter.Deserialise<ApplicationKeys>(json);
        }

        public override string ToString() {
            return JsonConverter.Serialize<ApplicationKeys>(this);
        }
        #endregion
    }
}