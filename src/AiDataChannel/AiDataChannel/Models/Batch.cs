﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AiDataChannel.Helpers;

namespace AiDataChannel.Models
{
    public class Batch
    {
        public string applicationKey { get; set; }
        public int numberOfEvents { get; set; }
        public Event[] events { get; set; }

        #region plumbing
        public static Batch CreateFromJson(string json) {
            return JsonConverter.Deserialise<Batch>(json);
        }

        public override string ToString() {
            return JsonConverter.Serialize<Batch>(this);
        }

        public override int GetHashCode() {
            int hash = 7;
            hash = 31 * hash + applicationKey.GetHashCode();
            hash = 31 * hash + numberOfEvents.GetHashCode();
            foreach (Event evnt in events) {
                hash = 31 * hash + evnt.GetHashCode();
            }
            return hash;
        }

        public override bool Equals(object obj) {
            if (this == obj) return true;
            if (obj != null && GetType().Equals(obj.GetType())) {
                Batch that = (Batch)obj;
                bool eventsAreSame = true;
                int i = 0;
                while (eventsAreSame && i < events.Length) {
                    if (!this.events[i].Equals(that.events[i])) eventsAreSame = false;
                    i++;
                }
                return (this.applicationKey == that.applicationKey &&
                    this.numberOfEvents == that.numberOfEvents &&
                    eventsAreSame);
            }
            else {
                return false;
            }
        }
        #endregion
    }
}