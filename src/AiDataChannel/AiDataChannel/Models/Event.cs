﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using AiDataChannel.Helpers;

namespace AiDataChannel.Models
{
    

    public class Event
    {
        public ObjectId _id { get; set; }
        public DateTime timeStamp { get; set; }
        public string sessionId { get; set; }
        public string clientDip { get; set; }
        public string componentName { get; set; }

        public string eventType { get; set; }
        public string severity { get; set; }
        public string message { get; set; }
        public Metric[] metrics { get; set; }

        #region plubming
        public static Event CreateFromJson(string json) {
            return JsonConverter.Deserialise<Event>(json);
        }

        public static Event[] CreateArrayFromJson(string json) {
            return JsonConverter.Deserialise<Event[]>(json);
        }

        public override string ToString() {
            return JsonConverter.Serialize<Event>(this);
        }

        public override int GetHashCode() {
            int hash = 7;
            hash = 31 * hash + _id.GetHashCode();
            hash = 31 * hash + timeStamp.GetHashCode();
            hash = 31 * hash + sessionId.GetHashCode();
            hash = 31 * hash + clientDip.GetHashCode();
            hash = 31 * hash + componentName.GetHashCode();
            hash = 31 * hash + eventType.GetHashCode();
            hash = 31 * hash + severity.GetHashCode();
            hash = 31 * hash + message.GetHashCode();
            foreach (Metric metric in metrics) {
                hash = 31 * hash + metric.GetHashCode();
            }
            return hash;
        }

        public override bool Equals(object obj) {
            if (this == obj) return true;
            if (obj != null && GetType().Equals(obj.GetType())) {
                Event that = (Event)obj;                
                bool metricsAreSame = true;
                int i = 0;
                while (metricsAreSame && i < metrics.Length) {
                    if (!this.metrics[i].Equals(that.metrics[i])) metricsAreSame = false;
                    i++;
                }
                return (this._id.Equals(that._id) &&
                    this.timeStamp.Equals(that.timeStamp) &&
                    this.sessionId == that.sessionId &&
                    this.clientDip == that.clientDip &&
                    this.componentName == that.componentName &&
                    this.eventType == that.eventType &&
                    this.severity == that.severity &&
                    this.message == that.message &&
                    metricsAreSame);
            }
            else {
                return false;
            }
        }
        #endregion
    }

    
}