﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AiDataChannel.Helpers;

namespace AiDataChannel.Models
{
    public class Metric
    {
        public string name { get; set; }
        public object value { get; set; }

        #region plumbing
        public override int GetHashCode() {
            int hash = 7;
            hash = 31 * hash + name.GetHashCode();
            hash = 31 * hash + value.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj) {
            if (this == obj) return true;
            if (obj != null && GetType().Equals(obj.GetType())) {
                Metric that = (Metric)obj;
                return (this.name == that.name &&
                    this.value.Equals(that.value));
            }
            else {
                return false;
            }
        }

        public override string ToString() {
            return JsonConverter.Serialize<Metric>(this);
        }
        #endregion
    }
}