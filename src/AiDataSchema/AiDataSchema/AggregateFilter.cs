﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AiDataSchema
{
    public class AggregateFilter
    {
        public static string _ALLITEMS = "all";
        public string eventType { get; set; }
        public string component { get; set; }
        public string sessionId { get; set; }
        public string clientDip { get; set; }
        public string severity { get; set; }
        public ICollection<string> metrics { get; set; }
        public DateTime fromTime { get; set; }
        public DateTime toTime { get; set; }
        public TimePeriod aggregationUnit { get; set; }
    }
}
