﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace AiDataSchema
{
    public class MetricNameAvg
    {
        public Dictionary<string, double> Metric { get; set; }

        public MetricNameAvg()
        {
            Metric = new Dictionary<string, double>();
        }
    }

    public class AggregateResultsContainer
    {
        private Dictionary<DateTime, MetricNameAvg> _results = new Dictionary<DateTime, MetricNameAvg>();

        public void AddResult(DateTime fromTime, string metricName, double avgValue)
        {
            if (_results.ContainsKey(fromTime))
            {
                // the assumption is that this particular metricname hasnt been added to this particular datetime. each datetime will have only 1 of each metricname
                _results[fromTime].Metric.Add(metricName, avgValue);
            }
            else
            {
                // no entry exists for this datetime, so add the datetime entry and then the metricnameavg entry
                MetricNameAvg metricNameAvg = new MetricNameAvg();
                metricNameAvg.Metric.Add(metricName, avgValue);
                _results.Add(fromTime, metricNameAvg);
            }
        }

        public DataTable GetAsTable(TimePeriod tp)
        {
            DataTable table = new DataTable();
            table.Columns.Add("FromTime");
            string dateFormat = "";
            switch (tp) {
                case TimePeriod.dayOfYear:
                case TimePeriod.week:
                    dateFormat = "d";
                    break;
                case TimePeriod.month:
                    dateFormat = "MMM yyyy";
                    break;
                case TimePeriod.year:
                    dateFormat = "yyyy";
                    break;
                default:
                    dateFormat = "d";
                    break;
            }


            foreach (KeyValuePair<DateTime, MetricNameAvg> row in _results.OrderBy(r => r.Key)) {                
                DateTime fromTime = row.Key;
                MetricNameAvg metrics = row.Value;
                DataRow dataRow = table.NewRow();
                dataRow["FromTime"] = fromTime.ToString(dateFormat);

                foreach (var metric in metrics.Metric.ToList())
                {
                    string metricName = metric.Key;
                    double aggregateAvg = metric.Value;

                    if (table.Columns.IndexOf(metricName) == -1)
                    {
                        // column not found, so add it, then add the row
                        table.Columns.Add(metricName);
                    }

                    dataRow[metricName] = String.Format("{0:0.00}", aggregateAvg);
                    
                }

                table.Rows.Add(dataRow);
            }

            return table;
        }
    }
}
