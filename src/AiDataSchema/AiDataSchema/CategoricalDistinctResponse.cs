﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AiDataSchema
{
    public class CategoricalDistinctValue
    {
        public string value { get; set; }
    }

    public class CategoricalDistinctResult
    {
        public CategoricalDistinctValue _id { get; set; }      
    }

    public class CategoricalDistinctResponse
    {
        public ICollection<CategoricalDistinctResult> result { get; set; }
        public bool ok { get; set; }
    }
}
