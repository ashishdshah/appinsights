﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using System.Configuration;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.IO;

namespace AiDataSchema
{
    public enum TimePeriod 
    {
        year, 
        month, 
        week, 
        dayOfYear
    };

    public class EventStreamSchema
    {
        private MongoDatabase _db;
        private MongoCollection _events;

        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public IEnumerable<string> SessionIds { get; set; }
        public IEnumerable<string> ClientDips { get; set; }
        public IEnumerable<string> Components { get; set; }
        public IEnumerable<string> EventTypes { get; set; }
        public IEnumerable<string> Severities { get; set; }
        public IEnumerable<MetricSchema> Metrics { get; set; }

        private static string GenerateCollectionName(string applicationKey) {
            return applicationKey + "_events";
        }

        public CategoricalDistinctResponse GetCategoricalDistinct(string metricName) {
            var commandDocument = new CommandDocument
            {
                {"aggregate", _events.Name},
                {
                    "pipeline", new BsonArray
                    {
			            new BsonDocument {{"$unwind", "$metrics"}},
			            new BsonDocument {{"$match", new BsonDocument {{"metrics.name", metricName}}}},
			            new BsonDocument {{ "$group", new BsonDocument {{
				            new BsonElement ("_id", new BsonDocument {{"value", "$metrics.value"}})
                            }}}}
                    }
                }
            };
            var result = _db.RunCommand(commandDocument);
            return BsonSerializer.Deserialize<CategoricalDistinctResponse>(result.Response);
        }

        public NumericalMaxMinResponse GetNumericalMaxMin(string metricName) {            
            var commandDocument = new CommandDocument
            {
                {"aggregate", _events.Name},
                {
                    "pipeline", new BsonArray
                    {
			            new BsonDocument {{"$unwind", "$metrics"}},
			            new BsonDocument {{"$match", new BsonDocument {{"metrics.name", metricName}}}},
			            new BsonDocument {{ "$group", new BsonDocument {{
				            new BsonElement ("_id", "$metrics.name"),
                            new BsonElement ("minimum", new BsonDocument{{"$min", "$metrics.value"}}),
                            new BsonElement ("maximum", new BsonDocument{{"$max", "$metrics.value"}})
                            }}}}
                    }
                }
            };            
            var result = _db.RunCommand(commandDocument);
            return BsonSerializer.Deserialize<NumericalMaxMinResponse>(result.Response);
        }

        // TODO: When possible convert to using aggregation f/w
        public IEnumerable<TypeResult> FindMetricsType() {
            string map = @"
                function() {
	                for(var i=0; i<this.metrics.length; i++) {
		                val = this.metrics[i].value;
		                typeVal = typeof val;		                
		                emit(this.metrics[i].name, {type: typeVal});
	                }
                }";

            string reduce = @"        
                function(key, values) {
	                res = values[0];
	                for(var i=1; i<values.length; i++) {
		                if(values[i].type != res.type) {
			                res.type = 'mixed';			                
		                }		            
	                }
	                return res;
                }";

            var options = new MapReduceOptionsBuilder();
            options.SetOutput(MapReduceOutput.Inline);
            var results = _events.MapReduce(map, reduce, options);
            return results.GetResultsAs<TypeResult>();
        }

        public NumericalAggregateResults AggregateNumericMetric(string metricName, AggregateFilter af)
        {
            var commandDocument = new CommandDocument
            {
                {"aggregate", _events.Name},
                {
                    "pipeline", new BsonArray
                    {
                        new BsonDocument {{"$match", new BsonDocument {{"timeStamp", new BsonDocument {{ new BsonElement("$gte", af.fromTime), new BsonElement("$lte", af.toTime) }} }}}},
			            new BsonDocument {{"$unwind", "$metrics"}},
			            new BsonDocument {{"$match", GetFilterBsonDocument(metricName, af)}},
			            new BsonDocument {{ "$group", new BsonDocument {{
				            new BsonElement ("_id", new BsonDocument {{new BsonElement("originYear", new BsonDocument {{"$year", "$timeStamp"}}), new BsonElement(af.aggregationUnit.ToString(), new BsonDocument {{"$" + af.aggregationUnit.ToString(), "$timeStamp"}})}}),
                            new BsonElement ("avg", new BsonDocument {{"$avg", "$metrics.value"}})
                            }}}}
                    }
                }
            };

            var result = _db.RunCommand(commandDocument);
            return BsonSerializer.Deserialize<NumericalAggregateResults>(result.Response);
        }

        private BsonDocument GetFilterBsonDocument(string metricName, AggregateFilter af)
        {
            BsonDocument filterDoc = new BsonDocument();

            filterDoc.Add(new BsonElement("metrics.name", metricName));

            if (af.eventType != AggregateFilter._ALLITEMS)
                filterDoc.Add(new BsonElement("eventType", af.eventType));

            if (af.component != AggregateFilter._ALLITEMS)
                filterDoc.Add(new BsonElement("component", af.component));

            if (af.severity != AggregateFilter._ALLITEMS)
                filterDoc.Add(new BsonElement("severity", af.severity));

            if (af.sessionId != AggregateFilter._ALLITEMS)
                filterDoc.Add(new BsonElement("sessionId", af.sessionId));

            if (af.clientDip != AggregateFilter._ALLITEMS)
                filterDoc.Add(new BsonElement("clientDip", af.clientDip));
            return filterDoc;
        }

        public CategoricalAggregateResults AggregateCategoricalMetric(string metricName, AggregateFilter af) {
            var commandDocument = new CommandDocument
            {
                {"aggregate", _events.Name},
                {
                    "pipeline", new BsonArray
                    {
                        new BsonDocument {{"$match", new BsonDocument {{"timeStamp", new BsonDocument {{ new BsonElement("$gte", af.fromTime), new BsonElement("$lte", af.toTime) }} }}}},
			            new BsonDocument {{"$unwind", "$metrics"}},
			            new BsonDocument {{"$match", GetFilterBsonDocument(metricName, af)}},
			            new BsonDocument {{ "$group", new BsonDocument {{
				            new BsonElement ("_id", new BsonDocument {{new BsonElement("value", "$metrics.value"), new BsonElement("originYear", new BsonDocument {{"$year", "$timeStamp"}}), new BsonElement(af.aggregationUnit.ToString(), new BsonDocument {{"$" + af.aggregationUnit.ToString(), "$timeStamp"}})}}),
                            new BsonElement ("count", new BsonDocument {{"$sum", 1}})
                            }}}}
                    }
                }
            };
            var result = _db.RunCommand(commandDocument);
            return BsonSerializer.Deserialize<CategoricalAggregateResults>(result.Response);
        }
        
        public EventStreamSchema(string appKey) {
            string connStr = ConfigurationManager.ConnectionStrings["mongoConnStr"].ConnectionString;
            MongoServer server = MongoServer.Create(connStr);
            _db = server.GetDatabase("AiEventStream");
            _events = _db.GetCollection(GenerateCollectionName(appKey));
            // TODO: check if _events count is 0 and handle it. Add a ValidateAppKey method.
        }

        public bool GetData() {
            try {
                MinDate = _events.FindMinValue("timeStamp").AsDateTime;
                MaxDate = _events.FindMaxValue("timeStamp").AsDateTime;

                var uniqSessionIds = _events.Distinct("sessionId"); // TODO: too many sids for a dropdown.

                Func<BsonValue, string> stringOrNull = x => x.GetType() == typeof(MongoDB.Bson.BsonNull) ? "" : x.AsString;

                SessionIds = uniqSessionIds.Select<BsonValue, string>(stringOrNull);

                var uniqClientDips = _events.Distinct("clientDip"); // TODO: too many dips for a dropdown.
                ClientDips = uniqClientDips.Select<BsonValue, string>(stringOrNull);

                var uniqComponents = _events.Distinct("componentName");
                Components = uniqComponents.Select<BsonValue, string>(stringOrNull);

                var uniqEventTypes = _events.Distinct("eventType");
                EventTypes = uniqEventTypes.Select<BsonValue, string>(stringOrNull);

                var uniqSeverities = _events.Distinct("severity");
                Severities = uniqSeverities.Select<BsonValue, string>(stringOrNull);

                List<MetricSchema> metrics = new List<MetricSchema>();
                foreach (TypeResult tr in FindMetricsType()) {
                    MetricSchema ms = new MetricSchema();
                    ms.MetricName = tr._id;
                    if (tr.value.type == "number") {
                        ms.MetricType = MetricDataType.NUMERICAL;
                        var response = GetNumericalMaxMin(ms.MetricName);
                        double max = response.result.ElementAt<NumericalMaxMinResult>(0).maximum;
                        double min = response.result.ElementAt<NumericalMaxMinResult>(0).minimum;
                        ms.Range = new double[] { min, max };
                        ms.Categories = new List<string>(0);
                    }
                    else if (tr.value.type == "string") {
                        ms.MetricType = MetricDataType.CATEGORICAL;
                        ms.Range = new double[] { 0, 0 };
                        var response = GetCategoricalDistinct(ms.MetricName);
                        ms.Categories = new List<string>(response.result.Count);
                        foreach (CategoricalDistinctResult res in response.result) {
                            ms.Categories.Add(res._id.value);
                        }
                    }
                    else if (tr.value.type == "mixed") {
                        ms.MetricType = MetricDataType.MIXED;
                        ms.Range = new double[] { 0, 0 };
                        ms.Categories = new List<string>(0);
                    }
                    metrics.Add(ms);
                }
                Metrics = metrics.AsEnumerable<MetricSchema>();
                return true;
            }
            catch (EmptyCollectionException) {
                return false;
            }
        }
    }
}
