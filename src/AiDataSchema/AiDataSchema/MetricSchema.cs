﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AiDataSchema
{
    public enum MetricDataType 
    {
        NUMERICAL,
        CATEGORICAL,
        MIXED
    }

    public class MetricSchema
    {
        public string MetricName { get; set; }
        public MetricDataType MetricType { get; set; }
        public double[] Range { get; set; }
        public ICollection<string> Categories { get; set; }
    }
}
