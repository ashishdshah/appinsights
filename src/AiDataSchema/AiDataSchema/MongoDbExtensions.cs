﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;

namespace AiDataSchema
{
    public class EmptyCollectionException : Exception { }
    public class InvalidFieldInCollectionException : Exception 
    {
        public InvalidFieldInCollectionException(string msg) : base(msg) { }
    }

    public static class MongoDbExtensions
    {        
        public static BsonValue FindMinValue(this MongoCollection collection, string fieldName, IMongoQuery query = null) {
            if (query == null) query = Query.Null;
            var cursor = collection.FindAs<BsonDocument>(query)
                         .SetSortOrder(SortBy.Ascending(fieldName))
                         .SetLimit(1)
                         .SetFields(fieldName);
            var totalItemsCount = cursor.Count();
            if (totalItemsCount == 0)
                throw new EmptyCollectionException();
            var item = cursor.Single();
            if (!item.Contains(fieldName))
                throw new InvalidFieldInCollectionException(String.Format("Field '{0}' can't be find within '{1}' collection", fieldName, collection.Name));
            return item.GetValue(fieldName);
        }

        public static BsonValue FindMaxValue(this MongoCollection collection, string fieldName, IMongoQuery query = null) {
            if (query == null) query = Query.Null;
            var cursor = collection.FindAs<BsonDocument>(query)
                         .SetSortOrder(SortBy.Descending(fieldName))
                         .SetLimit(1)
                         .SetFields(fieldName);
            var totalItemsCount = cursor.Count();
            if (totalItemsCount == 0)
                throw new EmptyCollectionException();
            var item = cursor.Single();
            if (!item.Contains(fieldName))
                throw new InvalidFieldInCollectionException(String.Format("Field '{0}' can't be find within '{1}' collection", fieldName, collection.Name));
            return item.GetValue(fieldName);
        }
    }
}
