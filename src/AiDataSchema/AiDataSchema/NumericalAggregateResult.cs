﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AiDataSchema
{
    public class NumericalAggregateId
    {   
        private int _dayOfYear;
        private int _month;
        private int _week;

        public int originYear { get; set; }
        public int year { get; set; }
        public int month {
            get {
                if (_month == 0)
                    return 1;
                else
                    return _month;
            }

            set {
                _month = value;
            }
        }

        public int week {

            get
            {
                return _week;
            }

            set
            {
                _week = value;
                dayOfYear = _week * 7;
            }
        }

        public int dayOfYear { 
            get {
                if (_dayOfYear > 0) {
                    DateTime theDate = new DateTime(originYear, 1, 1).AddDays(_dayOfYear - 1);
                    return theDate.Day;
                }
                else {
                    return 1;
                }
            }
            
            set {
                _dayOfYear = value;
                if (_dayOfYear > 0) {
                    DateTime theDate = new DateTime(originYear, 1, 1).AddDays(_dayOfYear - 1);
                    _month = theDate.Month;
                }
            }
        }
    }

    public class NumericalAggregateResult
    {
        public NumericalAggregateId _id { get; set; }
        public double avg { get; set; }
    }

    public class NumericalAggregateResults
    {
        public IEnumerable<NumericalAggregateResult> result { get; set; }
        public bool ok { get; set; }
    }
}
