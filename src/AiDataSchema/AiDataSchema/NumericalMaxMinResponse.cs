﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AiDataSchema
{
    public class NumericalMaxMinResult
    {
        public string _id { get; set; }
        public double minimum { get; set; }
        public double maximum { get; set; }
    }

    public class NumericalMaxMinResponse
    {
        public ICollection<NumericalMaxMinResult> result { get; set; }
        public bool ok { get; set; }
    }
}
