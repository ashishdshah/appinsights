﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace AiDataSchema
{
    public class TypeResultValue
    {
        public string type { get; set; }        
    }

    public class TypeResult
    {
        public string _id { get; set; }
        public TypeResultValue value { get; set; }
    }
}
