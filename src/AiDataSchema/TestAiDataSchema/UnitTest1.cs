﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using AiDataSchema;
using MongoDB.Driver.Builders;
using System.Configuration;
using MongoDB.Bson;

namespace TestAiDataSchema
{
    [TestClass]
    public class UnitTest1
    {
        private string apiKey = "a9654705a992c4c528452a3ae00b89f99";        

        [TestMethod]
        public void TestFindMetricsMinMax() {
            EventStreamSchema ess = new EventStreamSchema("a9654705a992c4c528452a3ae00b89f99");
            foreach (TypeResult mmr in ess.FindMetricsType()) {
                Console.WriteLine(mmr._id);
                Console.WriteLine("Type: " + mmr.value.type);                
            }
        }

        [TestMethod]
        public void TestGetData() {
            EventStreamSchema ess = new EventStreamSchema(apiKey);
            ess.GetData();
            Console.WriteLine("MinDate: {0}, MaxDate: {1}", ess.MinDate, ess.MaxDate);
            Console.WriteLine("Session IDs: " + ess.SessionIds.Aggregate((csvList, sid) =>  csvList + ", " + sid));
            Console.WriteLine("Client DIPs: " + ess.ClientDips.Aggregate((csvList, dip) => csvList + ", " + dip));
            Console.WriteLine("Components: " + ess.Components.Aggregate((csvList, comp) => csvList + ", " + comp));
            Console.WriteLine("EventTypes: " + ess.EventTypes.Aggregate((csvList, et) => csvList + ", " + et));
            Console.WriteLine("Severities: " + ess.Severities.Aggregate((csvList, sev) => csvList + ", " + sev));
            foreach (MetricSchema ms in ess.Metrics) {
                string cats = "";
                if(ms.Categories.Count > 0)
                    cats = ms.Categories.Aggregate((csvList, cat) => csvList + ", " + cat);
                Console.WriteLine("Metric: {0}, Type: {1}, Range: {2}, {3}, Categories: {4}", ms.MetricName, ms.MetricType, ms.Range[0], ms.Range[1], cats);
            }
        }


        [TestMethod]
        public void TestDistinct()
        {            
            EventStreamSchema ess = new EventStreamSchema("a9654705a992c4c528452a3ae00b89f99");
            CategoricalDistinctResponse response = ess.GetCategoricalDistinct("geoRegion");
            foreach(CategoricalDistinctResult res in response.result) {
                Console.WriteLine(res._id.value);
            }
        }        

        /*** changed the underlying function signature.. so change the test before it will work
        [TestMethod]
        public void TestCategoricalCount()
        {
            EventStreamSchema ess = new EventStreamSchema(apiKey);

            // year
            var result = ess.AggregateCategoricalMetric("geoRegion", TimePeriod.year, DateTime.Parse("2011-01-01"), DateTime.Parse("2011-12-01"));            

            // month
            result = ess.AggregateCategoricalMetric("geoRegion", TimePeriod.month, DateTime.Parse("2011-01-01"), DateTime.Parse("2011-12-01"));
            
            // week
            result = ess.AggregateCategoricalMetric("geoRegion", TimePeriod.week, DateTime.Parse("2011-01-01"), DateTime.Parse("2011-12-01"));            

            // dayofyear
            result = ess.AggregateCategoricalMetric("geoRegion", TimePeriod.dayOfYear, DateTime.Parse("2011-01-01"), DateTime.Parse("2011-12-01"));            
        }        

        [TestMethod]
        public void TestNumericAverage()
        {
            EventStreamSchema ess = new EventStreamSchema(apiKey);

            // year
            var result = ess.AggregateNumericMetric("cpu", TimePeriod.year, DateTime.Parse("2011-01-01"), DateTime.Parse("2011-12-01"));            

            // month
            result = ess.AggregateNumericMetric("cpu", TimePeriod.month, DateTime.Parse("2011-01-01"), DateTime.Parse("2011-12-01"));            

            // week
            result = ess.AggregateNumericMetric("queue", TimePeriod.week, DateTime.Parse("2011-01-01"), DateTime.Parse("2011-12-01"));           

            // dayofyear
            result = ess.AggregateNumericMetric("geoRegion", TimePeriod.dayOfYear, DateTime.Parse("2011-01-01"), DateTime.Parse("2011-12-01"));            
        }
        */
        

        [TestMethod]
        public void TestNumericalMaxMin()
        {            
            EventStreamSchema ess = new EventStreamSchema("a9654705a992c4c528452a3ae00b89f99");
            NumericalMaxMinResponse res = ess.GetNumericalMaxMin("cpu");
            foreach (NumericalMaxMinResult r in res.result) {
                Console.WriteLine("{0} max: {1}, min: {2}", r._id, r.maximum, r.minimum);
            }
        }        
    }
}
