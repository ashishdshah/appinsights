﻿// This file requires JQuery
// <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery/jquery-1.6.2.min.js"></script>

// Events will be posted on the data channel if there are more than 100 events queued or 2 mins are up.
var eventQueue = new Array();
var maxQueueLength = 100;
var maxInterval = 2 * 60 * 1000; //set to 2 mins
var t = setTimeout("postEventStream()", maxInterval);

//var appInsightsUrl = "http://www.streamviolet.com/api/DataStream";
var appInsightsUrl = "http://localhost:51420/DataStream";

var appKey = "";

$("div.log").ajaxError(function (e, jqxhr, settings, exception) {
    $(this).text("Triggered ajaxError handler.");
});

function setApplicationKey(ak) {
    appKey = ak;
}

function writeDataStream(event) {
    event._id = {
        _increment: 0,
        _machine: 0,
        _pid: 0,
        _timestamp: 0
    };
    if (event.timestamp == undefined) {
        event.timeStamp = "\/Date(" + (new Date()).valueOf() + ")\/"
    }
    if (event.componentName == undefined) {
        event.componentName = location.toString();
    }
    if (event.eventType == undefined) {
        event.eventType = "javascript";
    }
    
    eventQueue.push(event);    
    if (eventQueue.length >= maxQueueLength) {
        postEventStream();
    }
}

function postEventStream() {
    if (eventQueue.length > 0) {
        var eventStream = {
            applicationKey: appKey,
            events: eventQueue,
            numberOfEvents: eventQueue.length
        };
        var eventStreamStr = JSON.stringify(eventStream);

        var xmlHttp;
        if (window.XDomainRequest) {
            xmlHttp = new XDomainRequest();
            xmlHttp.open("POST", appInsightsUrl);
            xmlHttp.send("eventStream=" + eventStreamStr);
        }
        else {
            $.ajax({
                type: "POST",
                url: appInsightsUrl,
                xhrFields: { withCredentials: true },
                data: "eventStream=" + eventStreamStr                
            });
        }
        
        eventQueue = new Array();
    }
    
    clearTimeout(t);
    t = setTimeout("postEventStream()", maxInterval);
}