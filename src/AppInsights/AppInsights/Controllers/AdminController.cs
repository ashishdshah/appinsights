﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppInsights.Models;
using AppInsights.Helper;
using System.Web.Configuration;
using System.Configuration;

namespace AppInsights.Controllers
{
    public class AdminController : Controller
    {
        private AppInsightsContext _db = new AppInsightsContext();
        private TestDataGenerator _gen;

        public AdminController() {
            _gen = new TestDataGenerator(_db);
        }

        public ActionResult Index() {
            ViewBag.ConnStr = WebConfigurationManager.ConnectionStrings["AppInsightsContext"].ConnectionString;
            ViewBag.Count = 0; //_db.AiEvents.Count();
            return View();
        }

        public ActionResult Configs() {
            List<string> connStrs = new List<string>();
            foreach (ConnectionStringSettings css in WebConfigurationManager.ConnectionStrings) {
                connStrs.Add(css.Name + " " + css.ConnectionString);
            }

            List<string> appSettings = new List<string>();
            foreach (string key in WebConfigurationManager.AppSettings.Keys) {
                appSettings.Add(key + ": " + WebConfigurationManager.AppSettings[key]);
            }
            ViewBag.ConnStrs = connStrs;
            ViewBag.AppSettings = appSettings;
            return View();
        }

        public ActionResult GenerateSmallDataset() {
            _gen.SmallDataset();
            return RedirectToAction("Index");
        }

        public ActionResult GenerateMediumDataset() {
            _gen.MediumDataset();
            return RedirectToAction("Index");
        }

        public ActionResult GenerateLargeDataset() {
            _gen.LargeDataset();
            return RedirectToAction("Index");
        }

    }
}
