﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppInsights.Models;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Web.Configuration;

namespace AppInsights.Controllers
{ 
    [Authorize]
    public class ApplicationController : Controller
    {
        private AppInsightsContext _db = new AppInsightsContext();

        public ViewResult Index() {
            ViewBag.ApplicationsSelected = "selected";
            Customer user = _db.Customers.FirstOrDefault(c => c.Name == User.Identity.Name);
            var appaccounts = _db.AiApplication.Include(a => a.Customer).Where(a => a.CustomerId == user.Id);
            return View(appaccounts.ToList());
        }

        public ActionResult Details(int id) {
            ViewBag.ApplicationsSelected = "selected";
            Customer user = _db.Customers.FirstOrDefault(c => c.Name == User.Identity.Name);
            AiApplication appaccount = _db.AiApplication.FirstOrDefault(a => a.CustomerId == user.Id && a.Id == id);
            if (appaccount != null) {
                return View(appaccount);
            }
            else {
                return RedirectToAction("Index");
                // TODO: Should return to an error page
            }
        }

        public ActionResult Create() {
            ViewBag.ApplicationsSelected = "selected";
            return View();
        } 

        [HttpPost]
        public ActionResult Create(AiApplication appaccount) {
            ViewBag.ApplicationsSelected = "selected";
            if (ModelState.IsValid) {
                Customer user = _db.Customers.FirstOrDefault(c => c.Name == User.Identity.Name);
                appaccount.CustomerId = user.Id;
                appaccount.AccessKey = "a" + Guid.NewGuid().ToString("N");
                _db.AiApplication.Add(appaccount);
                _db.SaveChanges();
                if (!PostForm(WebConfigurationManager.AppSettings["AiDataChannelUrl"], "applicationKey=" + appaccount.AccessKey)) {
                    throw new Exception("Unable to set app key in mongo");
                }   
                return RedirectToAction("Index");  
            }            
            return View(appaccount);
        }

        public static bool PostForm(string url, string appKey) {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";            
            byte[] bytedata = Encoding.UTF8.GetBytes(appKey);
            webRequest.ContentLength = bytedata.Length;

            Stream requestStream = webRequest.GetRequestStream();
            requestStream.Write(bytedata, 0, bytedata.Length);
            requestStream.Close();

            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            if (webResponse.StatusCode == HttpStatusCode.OK)
                return true;
            else
                return false;
        }
                
        public ActionResult Edit(int id) {
            ViewBag.ApplicationsSelected = "selected";
            Customer user = _db.Customers.FirstOrDefault(c => c.Name == User.Identity.Name);
            AiApplication appaccount = _db.AiApplication.FirstOrDefault(a => a.CustomerId == user.Id && a.Id == id);
            return View(appaccount);
        }

        [HttpPost]
        public ActionResult Edit(AiApplication appaccount) {
            ViewBag.ApplicationsSelected = "selected";
            if (ModelState.IsValid) {
                _db.Entry(appaccount).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(appaccount);
        }

        public ActionResult Delete(int id) {
            ViewBag.ApplicationsSelected = "selected";
            Customer user = _db.Customers.FirstOrDefault(c => c.Name == User.Identity.Name);
            AiApplication appaccount = _db.AiApplication.FirstOrDefault(a => a.CustomerId == user.Id && a.Id == id);
            return View(appaccount);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id) {
            throw new NotImplementedException();

            //ViewBag.ApplicationsSelected = "selected";
            //Customer user = _db.Customers.FirstOrDefault(c => c.Name == User.Identity.Name);
            
            //var events = _db.AiEvents.Where(e => e.AiApplicationId == id && e.AiApplication.CustomerId == user.Id);
            //events.ToList().ForEach(e => _db.AiEvents.Remove(e));
            //_db.SaveChanges();

            //AiApplication appaccount = _db.AiApplication.FirstOrDefault(a => a.Id == id && a.CustomerId == user.Id);
            //_db.AiApplication.Remove(appaccount);
            //_db.SaveChanges();

            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}