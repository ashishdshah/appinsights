﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppInsights.Models;

namespace AppInsights.Controllers
{ 
    public class EventController : Controller
    {
        private AppInsightsContext _db = new AppInsightsContext();

        // GET /Event/?accesstoken=thisisaccesstoken&filter.messagekeywords=to&filter.names[0]=name1&filter.values[0]=value1&filter.names[1]=name2&filter.values[1]=value2
        public JsonResult Index(string accessToken, int? id = null, EventFilter filter = null) {
            throw new NotImplementedException();            
        }

        // DELETE /Event?accesstoken=thisisaccesstoken&id=1
        [HttpDelete]
        public JsonResult Index(string accessToken, int id) {
            throw new NotImplementedException();
        }

        // POST /Event/
        // customerKey = key here
        // applicationKey = key here
        // aievent.componentName = name here
        // aievent.message = some message here
        // aievent.severity.label = label here       
        // aievent.sessionid = id here
        // aievent.timestamp = ts
        // aievent.metricName = cpu
        // aievrnt.metricValue = 45
        // aievent.customFields[0].Name = name1
        // aievent.customFields[0].Value = value1
        // aievent.customFields[1].Name = name1
        // aievent.customFields[1].Value = value1
        // Comment out [RequireHttps] when testing with curl
        // curl --data 'customerKey=key1&applicationKey=app1key&aievent.componentName=cnamehere&aievent.message=msghere&aievent.severity.label=debug&aievent.metricname=cpu&aievent.metricvalue=45&aievent.sessionId=1&aievent.timestamp=1/3/2012&aievent.customFields[0].Name=name1&aievent.customFields[0].Value=value1&aievent.CustomFields[1].Name=name2&aievent.CustomFields[1].Value=value2' http://localhost:49540/Event
        //[RequireHttps]
        //[HttpPost]
        //public ActionResult Index(string customerKey, string applicationKey, AiEvent aiEvent) {
        //    var vapp = from a in _db.AiApplication
        //               join c in _db.Customers on a.CustomerId equals c.Id
        //               where c.Key == customerKey && a.AccessKey == applicationKey
        //               select a;
        //    AiApplication app = vapp.FirstOrDefault();
        //    if (app != null) {
        //        string label = aiEvent.Severity.Label.ToLowerInvariant();
        //        Severity sev = _db.Serverities.FirstOrDefault(s => s.Label == label);
        //        if (sev != null) {
        //            aiEvent.Severity = sev;
        //        }
        //        else {
        //            aiEvent.Severity = _db.Serverities.Find(2); // TODO: This should not be hard coded to id 2
        //        }
        //        aiEvent.AiApplicationId = app.Id;
        //        _db.AiEvents.Add(aiEvent);
        //        _db.SaveChanges();
        //        // TODO return the GET URL for this event when GET /Event is implemented
        //        return new EmptyResult();
        //    }
        //    else {
        //        return new HttpStatusCodeResult(400);                
        //    }
        //}
        
        // TODO: Do we need any update API? I think not..

        protected override void Dispose(bool disposing) {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}