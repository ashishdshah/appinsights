﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppInsights.Models;

namespace AppInsights.Controllers
{
    public class HomeController : Controller
    {
        private AppInsightsContext _db = new AppInsightsContext();

        public ActionResult Index() {
            ViewBag.HomeSelected = "selected";            
            return View();
        }

        public ActionResult About() {
            return View();
        }        
        
        public ActionResult Contact() {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(Contact contact) {
            if (ModelState.IsValid) {
                _db.Contacts.Add(contact);
                _db.SaveChanges();
                ViewBag.Notice = "Thanks we'll reach out to you as soon as we see this!";
                ModelState.Clear();
            }
            return View();
        }

        public ActionResult Learn() {
            return View();
        }

        public ActionResult Blogs() {
            ViewBag.BlogsSelected = "selected";
            return View();
        }

        public ActionResult Developers() {
            ViewBag.DevelopersSelected = "selected";
            return View();
        }

        public ActionResult Support() {            
            InitializeSupportPage();
            return View(new SupportTicket());
        }

        private void InitializeSupportPage() {
            ViewBag.SupportSelected = "selected";
            List<object> sevs = new List<object> {
                new { value = "High", text = "High"},
                new {value = "Medium", text = "Medium"},
                new {value = "Low", text = "Low" }
            };
            ViewBag.Sevs = sevs;

            List<object> areas = new List<object> {
                new { value = "DataChannel", text = "Metrics Ingress" },
                new {value = "Reporting", text = "Report Viewing" }
            };
            ViewBag.Areas = areas;
        }

        [HttpPost]
        public ActionResult Support(SupportTicket ticket) {
            InitializeSupportPage();
            if (ModelState.IsValid) {
                _db.SupportTickets.Add(ticket);
                _db.SaveChanges();
            }
            ViewBag.Notice = "Your ticket has been filed with ticket number " + ticket.Id;
            ModelState.Clear();
            return View(new SupportTicket());
        }
    }
}
