﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppInsights.Models;
using System.Web.Helpers;
using System.Globalization;
using AppInsights.Helper;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO;
using System.Net;
using AiDataSchema;

namespace AppInsights.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private AppInsightsContext _db = new AppInsightsContext();
        private Customer _user;
        private IQueryable<AiApplication> _userApps;

        private void InitializeUserData() {
            _user = _db.Customers.FirstOrDefault(c => c.Name == User.Identity.Name);
            _userApps = _db.AiApplication.Include(a => a.Customer).Where(a => a.CustomerId == _user.Id);
        }

        public ActionResult Index() {
            InitializeUserData();
            if (_userApps.Count() > 0) {
                AiApplication firstApp = _userApps.First();
                return RedirectToAction("Events", new {appKey = firstApp.AccessKey});
            }
            ViewBag.Notice = "You do not have any applications.";
            return View();
        }

        public ActionResult Events(string appKey) {
            InitializeUserData();
            if (_userApps.FirstOrDefault(a => a.AccessKey == appKey) == null) {
                Response.StatusCode = (int)HttpStatusCode.Forbidden;
                return new EmptyResult();
            }
            
            EventParams eventParams = new EventParams();
            eventParams.ApplicationKey = appKey;
            EventStreamSchema ess = new EventStreamSchema(appKey);
            if (ess.GetData()) {
                eventParams.Metrics.Add(ess.Metrics.ElementAt<MetricSchema>(0).MetricName);
                ViewBag.EventStreamSchema = ess;
                AiApplication accountSelected = _userApps.Where(a => a.AccessKey == eventParams.ApplicationKey).First();
                ViewBag.CurrentApp = accountSelected;
                ViewBag.AllApps = _userApps.ToList();
                DataTable data = GenerateTable(eventParams, ess);
                ViewBag.Data = data;
                Session["data"] = data;
                return View(eventParams);
            }
            else {
                ViewBag.Notice = "Your application has not logged any metrics.";
                return View("Index");
            }
        }

        private DataTable GenerateTable(EventParams eventParams, EventStreamSchema ess) {
            #region OldTestTableCode
            /*
            TimeSeriesTable table = new TimeSeriesTable("testtable");
            
            // Generate the table schema
            foreach(string metric in eventParams.Metrics) {
                MetricSchema ms = ess.Metrics.First(m => m.MetricName == metric);
                if (ms.MetricType == MetricDataType.NUMERICAL) {
                    table.Columns.Add(new DataColumn(ms.MetricName, Type.GetType("System.String")));
                }
                else if (ms.MetricType == MetricDataType.CATEGORICAL) {
                    foreach (string cat in ms.Categories) {
                        string colName = ms.MetricName + "_" + cat;
                        table.Columns.Add(new DataColumn(colName, Type.GetType("System.String")));
                    }
                }
                else if (ms.MetricType == MetricDataType.MIXED) {
                    throw new Exception("Mixed type");
                }
            }

            // Fetch the data based on the filters
            foreach (string metric in eventParams.Metrics)
            {
                MetricSchema ms = ess.Metrics.First(m => m.MetricName == metric);
                if (ms.MetricType == MetricDataType.NUMERICAL)
                {

                    NumericalAggregateResults nars = ess.AggregateNumericMetric(ms.MetricName, eventParams.TimePeriod, eventParams.FromDate, eventParams.ToDate);
                    foreach (NumericalAggregateResult nar in nars.result)
                    {
                        DataRow row = table.NewRow();
                        row["FromTime"] = new DateTime(nar._id.originYear, nar._id.month, nar._id.dayOfYear);
                        row[ms.MetricName] = nar.avg;
                        table.Rows.Add(row);
                    }
                }
                else if (ms.MetricType == MetricDataType.CATEGORICAL)
                {
                    // categorical
                }
            }

            return table;
            */
            #endregion

            // collect all results in the container
            AggregateResultsContainer resultsContainer = new AggregateResultsContainer();
            foreach (string metric in eventParams.Metrics)
            {
                MetricSchema ms = ess.Metrics.First(m => m.MetricName == metric);
                if (ms.MetricType == MetricDataType.NUMERICAL)
                {
                    NumericalAggregateResults nars = ess.AggregateNumericMetric(ms.MetricName, eventParams.GetFilter());
                    foreach (NumericalAggregateResult nar in nars.result)
                    {
                        DateTime dt = new DateTime(nar._id.originYear, nar._id.month, nar._id.dayOfYear);
                        resultsContainer.AddResult(dt, ms.MetricName, nar.avg);
                    }
                }
                else if (ms.MetricType == MetricDataType.CATEGORICAL)
                {
                    // categorical: combine metricname with distinct categories as the name
                    CategoricalAggregateResults cars = ess.AggregateCategoricalMetric(ms.MetricName, eventParams.GetFilter());
                    foreach (CategoricalAggregateResult car in cars.result)
                    {
                        string computedMetricName = ms.MetricName + "_" + car._id.value.Replace(" ", "");
                        DateTime dt = new DateTime(car._id.originYear, car._id.month, car._id.dayOfYear);
                        resultsContainer.AddResult(dt, computedMetricName, car.count);

                    }
                }
            }

            // all results are in container, now create the table
            DataTable data = resultsContainer.GetAsTable(eventParams.TimePeriod);
            return data;
        }

        [HttpPost]
        public ActionResult Events(EventParams eventParams) {
            InitializeUserData();
            if (_userApps.FirstOrDefault(a => a.AccessKey == eventParams.ApplicationKey) == null)
            {
                Response.StatusCode = (int)HttpStatusCode.Forbidden;
                return new EmptyResult();
            }

            EventStreamSchema ess = new EventStreamSchema(eventParams.ApplicationKey);
            if (ess.GetData()) {
                // Generate a time series table of events based on the event params
                DataTable data = GenerateTable(eventParams, ess);
                ViewBag.Data = data;
                ViewBag.EventStreamSchema = ess;
                AiApplication accountSelected = _userApps.Where(a => a.AccessKey == eventParams.ApplicationKey).First();
                ViewBag.CurrentApp = accountSelected;
                ViewBag.AllApps = _userApps.ToList();
                Session["data"] = data;
                return View(eventParams);
            }
            else {
                ViewBag.Notice = "Your application has not logged any metrics.";
                return View("Index");
            }
        }

        public FileStreamResult DownloadCsv()
        {
            DataTable data = Session["data"] as DataTable;
            string csvData = data.ConvertToCsv();
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=Export.csv");
            var sw = new StreamWriter(new MemoryStream());              
            sw.WriteLine(csvData);         
            sw.Flush();
            sw.BaseStream.Seek(0, SeekOrigin.Begin);
            return new FileStreamResult(sw.BaseStream, "text/csv");
        }
        
        //public ActionResult MetricsLog(int appId) {
        //    InitializeUserData();
        //    // TODO: validate whether the user owns this application or not.
        //    MetricLogParam mp = new MetricLogParam();
        //    mp.ApplicationId = appId;
        //    if (MetricsLogWorker(mp))
        //        return View("MetricsLog", mp);
        //    else
        //        return View("Index");
        //}
         
        [HttpPost]
        public ActionResult MetricsLog(MetricLogParam mlParam) {
            InitializeUserData();
            if (MetricsLogWorker(mlParam))
                return View("MetricsLog", mlParam);
            else
                return View("Index");
        }

        private bool MetricsLogWorker(MetricLogParam mlp) {
            //AiApplication accountSelected = _userApps.Where(a => a.Id == mlp.ApplicationId).First();
            //ViewBag.AppList = new SelectList(_userApps.ToList(), "Id", "Name");

            //// get the distinct MetricNames
            //var mn = new MultiSelectList(accountSelected.AiEvents.Select(evt => new { MetricNameId = evt.MetricName, evt.MetricName }).Where(evt => !string.IsNullOrEmpty(evt.MetricName)).Distinct(), "MetricNameId", "MetricName", mlp.SelectedMetrics);
            //if(mn.Count() == 0) {
            //    ViewBag.Notice = "Your application has not logged any metrics.";
            //    return false;
            //}
            //else if (mlp.SelectedMetrics.Count == 0) {
            //    mn.First().Selected = true;
            //    mlp.SelectedMetrics.Add(mn.First().Value);
            //}
            //ViewBag.MetricList = mn;

            //PlotOptions ops;            
            //if (mlp.MeasureToShow == "trend") {
            //    List<PlotData> plots = mlp.GetMetricData(_db, out ops);
            //    ViewBag.PlotData = Utils.SerializeToJson<List<PlotData>>(plots);
            //    ViewBag.PlotOptions = Utils.SerializeToJson<PlotOptions>(ops);
            //    ViewBag.ChartTitle = "Average metric value";
            //}
            //else if (mlp.MeasureToShow == "statistics") {
            //    List<ErrorPlotData> plots = mlp.GetMetricErrorPlotData(_db, out ops);
            //    ViewBag.PlotData = Utils.SerializeToJson<List<ErrorPlotData>>(plots);                
            //    ViewBag.PlotOptions = Utils.SerializeToJson<PlotOptions>(ops);
            //    ViewBag.ChartTitle = "Average metric value with 1 standard deviation upper and lower bounds";
            //}
            return true;
        }

        public ActionResult ErrorLog(int? appId)
        {
            InitializeUserData();

            if (appId == null)
            {
                AiApplication firstApp = _userApps.First();
                appId = firstApp.Id;
            }

            MetricLogParam mp = new MetricLogParam();
            mp.ApplicationId = (int)appId;
            if (ErrorLogWorker(mp))
                return View("ErrorLog", mp);
            else
                return View("Index");
        }

        [HttpPost]
        public ActionResult ErrorLog(MetricLogParam mp)
        {
            InitializeUserData();
            mp.MeasureToShow = "trend";
            if (ErrorLogWorker(mp))
                return View("ErrorLog", mp);
            else
                return View("Index");        
        }

        private bool ErrorLogWorker(MetricLogParam mlp) {
            //mlp.MeasureToShow = "trend";
            //AiApplication accountSelected = _userApps.Where(a => a.Id == mlp.ApplicationId).First();
            //ViewBag.AppList = new SelectList(_userApps.ToList(), "Id", "Name");

            //// get the distinct severity list
            //var sevList = _db.Serverities.ToList();
            //var sevMulti = new MultiSelectList(sevList, "Id", "Label", mlp.SelectedSeverity);
            //if (mlp.SelectedSeverity.Count == 0) {
            //    sevMulti.First().Selected = true;
            //    mlp.SelectedSeverity.Add(Int32.Parse(sevMulti.First().Value));
            //}
            //ViewBag.SeverityList = sevMulti;

            //// get the distinct components
            //var compList = accountSelected.AiEvents.Select(evt => new { ComponentNameId = evt.ComponentName, evt.ComponentName }).Where(evt => !string.IsNullOrEmpty(evt.ComponentName)).Distinct();
            //var compMulti = new MultiSelectList(compList, "ComponentNameId", "ComponentName", mlp.SelectedComponents);
            //if (compMulti.Count() == 0) {
            //    ViewBag.Notice = "None of your components have logged anything.";
            //    return false;
            //}
            //if (mlp.SelectedComponents.Count == 0) {
            //    compMulti.First().Selected = true;
            //    mlp.SelectedComponents.Add(compMulti.First().Value);
            //}
            //ViewBag.ComponentList = compMulti;

            //PlotOptions ops;
            //if (mlp.MeasureToShow == "trend") {
            //    List<PlotData> plots = mlp.GetLogData(_db, out ops);
            //    ViewBag.PlotData = Utils.SerializeToJson<List<PlotData>>(plots);
            //    ViewBag.PlotOptions = Utils.SerializeToJson<PlotOptions>(ops);
            //    ViewBag.ChartTitle = "Count of logs";
            //}            
            return true;
        }
    }
}