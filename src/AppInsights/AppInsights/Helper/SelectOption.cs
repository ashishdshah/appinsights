﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Helper
{
    public class SelectOption
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public static ICollection<SelectOption> Create(IEnumerable<string> values, bool allNeeded) {
            List<SelectOption> options = new List<SelectOption>();
            if(allNeeded) options.Add(new SelectOption { Id = "all", Name = "all" });
            foreach (string value in values) {
                options.Add(new SelectOption { Id = value, Name = value });
            }
            return options;
        }
    }
}