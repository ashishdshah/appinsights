﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppInsights.Models;
using System.Data;

namespace AppInsights.Helper
{
    public class TestDataGenerator
    {
        private AppInsightsContext _db;
        private List<AiApplication> _apps;
        private List<Customer> _custs;
        //private List<Severity> _sevs;
        //private Random _rnd;
        //private string[] _components;
        //private string[] _metrics;
        //private DateTime _from;
        //private DateTime _to;
        
        public TestDataGenerator(AppInsightsContext db) {
            _db = db;
            //_rnd = new Random();
            //_components = new string[]{ "Class1.Method1", "Class1.Method2" };
            //_metrics = new string[]{ "strange", "charmed", "top", "down" };
        }

        private void VerifyBaseDataExists() {
            _apps = _db.AiApplication.ToList();
            _custs = _db.Customers.ToList();
            //_sevs = _db.Serverities.ToList();
            if (_apps.Count >= 2 &&
                _custs.Count >= 2) {
                //_sevs.Count >= 5) 
            }
            else {
                AddBaseData();
                _apps = _db.AiApplication.ToList();
                _custs = _db.Customers.ToList();
                //_sevs = _db.Serverities.ToList();
            }
        }

        //private void DeleteAllEvents() {
        //    foreach (AiEvent evnt in _db.AiEvents) {
        //        _db.AiEvents.Remove(evnt);
        //    }
        //}

        //private DateTime RandomTimestamp() {
        //    long min = _from.Ticks;
        //    long max = _to.Ticks;
        //    byte[] buf = new byte[8];
        //    _rnd.NextBytes(buf);
        //    long longRand = BitConverter.ToInt64(buf, 0);
        //    long ticks = (Math.Abs(longRand % (max - min)) + min);
        //    return new DateTime(ticks);
        //}

        //private string RandomComponent() {
        //    return _components[_rnd.Next(_components.Length)];
        //}

        //public string RandomMetric() {
        //    return _metrics[_rnd.Next(_metrics.Length)];
        //}

        //public int RandomSevId() {
        //    //return _sevs.ElementAt(_rnd.Next(_sevs.Count())).Id;
        //    return _sevs[_rnd.Next(_sevs.Count)].Id;
        //}

        //private void GenerateEvents(int numEvents) {
        //    VerifyBaseDataExists();
        //    DeleteAllEvents();
        //    List<AiEvent> events = new List<AiEvent>();
        //    for (int i = 0; i < numEvents; i++) {
        //        AiEvent evnt = new AiEvent {
        //            AiApplicationId = _apps[0].Id,
        //            SessionId = "session id 1",
        //            SeverityId = RandomSevId(),
        //            MetricName = RandomMetric(),
        //            MetricValue = _rnd.NextDouble(),
        //            Timestamp = RandomTimestamp()
        //        };
        //        events.Add(evnt);
        //    }
        //    for (int i = 0; i < numEvents; i++) {
        //        AiEvent evnt = new AiEvent {
        //            //AiApplicationId = _apps[1].Id,
        //            AiApplicationId = _apps[0].Id,
        //            SessionId = "session id 1",
        //            ComponentName = RandomComponent(),
        //            Message = "msg here",
        //            SeverityId = RandomSevId(),
        //            Timestamp = RandomTimestamp()
        //        };
        //        events.Add(evnt);
        //    }
        //    events.ForEach(e => _db.AiEvents.Add(e));
        //    _db.SaveChanges();
        //}

        public void AddBaseData() {
            List<Customer> customers = new List<Customer> {
                new Customer { 
                    Name = "Test User 1",
                    Key = "key1",
                    Password = "password1", 
                    Email = "a@b.com", 
                    ConfirmPassword = "password1" 
                },
                new Customer { 
                    Name = "Test User 2", 
                    Key = "key2",
                    Password = "password2", 
                    Email = "c@d.com", 
                    ConfirmPassword = "password2" 
                }
            };
            customers.ForEach(u => _db.Customers.Add(u));
            _db.SaveChanges();

            List<AiApplication> appAccounts = new List<AiApplication> {
                new AiApplication { 
                    CustomerId = customers[0].Id, 
                    Name = "App 1 for User 1", 
                    AccessKey = "app1key" 
                },
                new AiApplication { 
                    CustomerId = customers[0].Id, 
                    Name = "App 2 for User 1", 
                    AccessKey = "app2key" 
                }
            };
            appAccounts.ForEach(aa => _db.AiApplication.Add(aa));
            _db.SaveChanges();

            //List<Severity> severities = new List<Severity> {
            //    new Severity { Label = "debug" },
            //    new Severity { Label = "info" },
            //    new Severity { Label = "warning" },
            //    new Severity { Label = "error" },
            //    new Severity { Label = "fatal" }
            //};
            //severities.ForEach(s => _db.Serverities.Add(s));
            //_db.SaveChanges();
        }

        public void SmallDataset() {
            //_from = DateTime.UtcNow.AddDays(-7);
            //_to = DateTime.UtcNow;
            //GenerateEvents(20);            
            VerifyBaseDataExists();
        }

        public void MediumDataset() {
            //_from = DateTime.UtcNow.AddMonths(-2);
            //_to = DateTime.UtcNow;
            //GenerateEvents(200);
            VerifyBaseDataExists();
        }

        public void LargeDataset() {
            //_from = DateTime.UtcNow.AddYears(-2);
            //_to = DateTime.UtcNow;
            //GenerateEvents(2000);
            VerifyBaseDataExists();
        }

        public static TimeSeriesTable GenerateTestTable() {
            TimeSeriesTable eventsTable = new TimeSeriesTable("test_table");
            DataColumn col;

            col = new DataColumn("CPU", Type.GetType("System.Double"));
            eventsTable.Columns.Add(col);

            col = new DataColumn("DocType-PDF", Type.GetType("System.Int32"));
            eventsTable.Columns.Add(col);

            col = new DataColumn("DocType-DOCX", Type.GetType("System.Int32"));
            eventsTable.Columns.Add(col);

            DataRow row;
            row = eventsTable.NewRow();
            row["TimeStamp"] = DateTime.Now.AddHours(-5);
            row["CPU"] = 4.33;
            row["DocType-PDF"] = 32;
            row["DocType-DOCX"] = 1;
            eventsTable.Rows.Add(row);

            row = eventsTable.NewRow();
            row["TimeStamp"] = DateTime.Now.AddHours(-4);
            row["CPU"] = 6.7;
            row["DocType-PDF"] = 6;
            row["DocType-DOCX"] = 44;
            eventsTable.Rows.Add(row);

            row = eventsTable.NewRow();
            row["TimeStamp"] = DateTime.Now.AddHours(-3);
            row["CPU"] = 9.78;
            row["DocType-PDF"] = 3;
            row["DocType-DOCX"] = 5;
            eventsTable.Rows.Add(row);

            row = eventsTable.NewRow();
            row["TimeStamp"] = DateTime.Now.AddHours(-2);
            row["CPU"] = 20.4;
            row["DocType-PDF"] = 23;
            row["DocType-DOCX"] = 15;
            eventsTable.Rows.Add(row);

            row = eventsTable.NewRow();
            row["TimeStamp"] = DateTime.Now.AddHours(-1);
            row["CPU"] = 90;
            row["DocType-PDF"] = 23;
            row["DocType-DOCX"] = 556;
            eventsTable.Rows.Add(row);

            return eventsTable;
        }
    }
}