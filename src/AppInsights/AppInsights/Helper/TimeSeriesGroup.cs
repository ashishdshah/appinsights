﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Helper
{
    public abstract class TimeSeriesGroup
    {
        private Dictionary<DateTime, List<double>> _table;
        private Dictionary<DateTime, double> _groupedTable;
        private List<DateTime> _markers;

        protected DateTime _from;
        protected DateTime _to;

        
        protected abstract DateTime Hash(DateTime original);

        protected abstract List<DateTime> GenerateMarkers();

        protected void Initialize() {
            _table = new Dictionary<DateTime, List<double>>();
            _groupedTable = new Dictionary<DateTime, double>();
            if (_markers == null) {
                _markers = GenerateMarkers();
            }
            foreach (DateTime marker in _markers) {
                _table.Add(marker, new List<double>());
                _groupedTable.Add(marker, 0);
            }             
        }

        public void AddPoint(DateTime x, double y) {
            _table[Hash(x)].Add(y);
        }

        public void Clear() {
            _table.Clear();
            _groupedTable.Clear();
            Initialize();
        }

        public Func<ICollection<double>, double> GroupingFunction;

        public void Group() {
            foreach (DateTime key in _table.Keys) {
                _groupedTable[key] = GroupingFunction(_table[key]);
            }
        }

        public DateTime[] XValues {
            get {
                return _groupedTable.Keys.OrderBy(k => k).ToArray();
            }
        }

        public double[] YValues {
            get {
                List<double> yvals = new List<double>();
                foreach (DateTime key in _groupedTable.Keys.OrderBy(k => k)) {
                    yvals.Add(_groupedTable[key]);
                }
                return yvals.ToArray();
            }
        }
    }
}