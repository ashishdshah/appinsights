﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Helper
{
    public class TimeSeriesGroupByDay : TimeSeriesGroup
    {
        public TimeSeriesGroupByDay(DateTime from, DateTime to) {
            _from = from;
            _to = to;
            Initialize();
        }

        protected override DateTime Hash(DateTime original) {
            return new DateTime(original.Year, original.Month, original.Day);
        }

        protected override List<DateTime> GenerateMarkers() {
            List<DateTime> markers = new List<DateTime>();
            for (DateTime s = Hash(_from); s <= _to; s = s.AddDays(1)) {
                markers.Add(s);
            }
            return markers;
        }
    }
}