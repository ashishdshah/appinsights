﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Helper
{
    public class TimeSeriesGroupByMonth : TimeSeriesGroup
    {
        public TimeSeriesGroupByMonth(DateTime from, DateTime to) {
            _from = from;
            _to = to;
            Initialize();
        }

        protected override DateTime Hash(DateTime original) {
            return new DateTime(original.Year, original.Month, 1);
        }

        protected override List<DateTime> GenerateMarkers() {
            List<DateTime> markers = new List<DateTime>();
            for (DateTime s = Hash(_from); s <= _to; s = s.AddMonths(1)) {
                markers.Add(s);
            }
            return markers;
        }
    }
}