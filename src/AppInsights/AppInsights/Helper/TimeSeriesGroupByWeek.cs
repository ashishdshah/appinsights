﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Helper
{
    public class TimeSeriesGroupByWeek : TimeSeriesGroup
    {
        public TimeSeriesGroupByWeek(DateTime from, DateTime to) {
            _from = from;
            _to = to;
            Initialize();
        }

        protected override DateTime Hash(DateTime original) {
            DateTime tmp = DateTime.MinValue;
            switch (original.DayOfWeek) {
                case DayOfWeek.Sunday:
                    tmp = original;
                    break;
                case DayOfWeek.Monday:
                    tmp = original.AddDays(-1);
                    break;
                case DayOfWeek.Tuesday:
                    tmp = original.AddDays(-2);
                    break;
                case DayOfWeek.Wednesday:
                    tmp = original.AddDays(-3);
                    break;
                case DayOfWeek.Thursday:
                    tmp = original.AddDays(-4);
                    break;
                case DayOfWeek.Friday:
                    tmp = original.AddDays(-5);
                    break;
                case DayOfWeek.Saturday:
                    tmp = original.AddDays(-6);
                    break;
            }
            return new DateTime(tmp.Year, tmp.Month, tmp.Day);
        }

        protected override List<DateTime> GenerateMarkers() {
            List<DateTime> markers = new List<DateTime>();
            for (DateTime s = Hash(_from); s <= _to; s = s.AddDays(7)) {
                markers.Add(s);
            }
            return markers;
        }
    }
}