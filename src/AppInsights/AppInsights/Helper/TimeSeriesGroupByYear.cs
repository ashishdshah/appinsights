﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Helper
{
    public class TimeSeriesGroupByYear : TimeSeriesGroup
    {
        public TimeSeriesGroupByYear(DateTime from, DateTime to) {
            _from = from;
            _to = to;
            Initialize();
        }

        protected override DateTime Hash(DateTime original) {
            return new DateTime(original.Year, 1, 1);
        }

        protected override List<DateTime> GenerateMarkers() {
            List<DateTime> markers = new List<DateTime>();
            for (DateTime s = Hash(_from); s <= _to; s = s.AddYears(1)) {
                markers.Add(s);
            }
            return markers;
        }
    }
}