﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Helper
{
    public class TimeSeriesGroupFactory
    {
        public static TimeSeriesGroup Create(string type, DateTime from, DateTime to) {
            TimeSeriesGroup grp = null;
            switch (type) {
                case "day":
                    grp = new TimeSeriesGroupByDay(from, to);
                    break;
                case "week":
                    grp = new TimeSeriesGroupByWeek(from, to);
                    break;
                case "month":
                    grp = new TimeSeriesGroupByMonth(from, to);
                    break;
                case "year":
                    grp = new TimeSeriesGroupByYear(from, to);
                    break;
            };
            return grp;
        }
    }
}