﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Data;

namespace AppInsights.Helper
{
    public static class Utils
    {
        public static double Average(ICollection<double> arr) {
            if (arr.Count > 0) {
                return arr.Average();
            }
            return 0;
        }

        public static double StdDev(ICollection<double> arr) {
            if (arr.Count > 0) {
                double avg = arr.Average();
                return Math.Sqrt(arr.Average(v => Math.Pow(v - avg, 2)));
            }
            return 0;
        }

        public static long ConvertToUnixTime(DateTime dt) {
            TimeSpan t = (dt - new DateTime(1970, 1, 1).ToLocalTime());
            long timestamp = (long)t.TotalSeconds * 1000;
            return timestamp;
        }

        public static string SerializeToJson<T>(T obj) {
            var serializer = new DataContractJsonSerializer(obj.GetType());
            using (var ms = new MemoryStream()) {
                serializer.WriteObject(ms, obj);
                return Encoding.Default.GetString(ms.ToArray());
            }
        }

        public static double Count(ICollection<double> arr) {
            return arr.Count();
        }

        public static string ConvertToCsv(this DataTable table) {
            StringBuilder builder = new StringBuilder();
                        
            // write the header
            foreach (DataColumn col in table.Columns) {
                builder.Append(col.ColumnName + ",");
            }
            builder.Remove(builder.Length - 1, 1);
            builder.AppendLine();

            // write the data per row
            foreach (DataRow row in table.Rows) {
                foreach (DataColumn col in table.Columns) {
                    builder.Append(row[col.ColumnName] + ",");
                }
                builder.Remove(builder.Length - 1, 1);
                builder.AppendLine();
            }
            return builder.ToString();
        }
    }
}