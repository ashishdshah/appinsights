﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AppInsights.Models
{
    public class AiApplication
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Application Name")]
        public string Name { get; set; }

        [Display(Name = "Application Key")]
        public string AccessKey { get; set; }

        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        //public virtual ICollection<AiEvent> AiEvents { get; set; }
    }
}