﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace AppInsights.Models
{
    public class AppInsightsContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<AiApplication> AiApplication { get; set; }
        public DbSet<SupportTicket> SupportTickets { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        //public DbSet<AiEvent> AiEvents { get; set; }
        //public DbSet<Severity> Serverities { get; set; }
        //public DbSet<CustomField> CustomFields { get; set; }
        //public DbSet<MetricLogParam> MetricLogParams { get; set; }
    }
}