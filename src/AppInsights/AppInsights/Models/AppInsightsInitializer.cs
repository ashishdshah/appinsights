﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using AppInsights.Helper;

namespace AppInsights.Models
{
    public class AppInsightsInitializer : DropCreateDatabaseIfModelChanges<AppInsightsContext>
    {
        protected override void Seed(AppInsightsContext context) {
            //List<Customer> customers = new List<Customer> {
            //    new Customer { 
            //        Name = "Test User 1",
            //        Key = "key1",
            //        Password = "password1", 
            //        Email = "a@b.com", 
            //        ConfirmPassword = "password1" 
            //    },
            //    new Customer { 
            //        Name = "Test User 2", 
            //        Key = "key2",
            //        Password = "password2", 
            //        Email = "c@d.com", 
            //        ConfirmPassword = "password2" 
            //    }
            //};
            //customers.ForEach(u => context.Customers.Add(u));
            //context.SaveChanges();

            //List<AiApplication> appAccounts = new List<AiApplication> {
            //    new AiApplication { 
            //        CustomerId = customers[0].Id, 
            //        Name = "App 1 for User 1", 
            //        AccessKey = "app1key" 
            //    },
            //    new AiApplication { 
            //        CustomerId = customers[0].Id, 
            //        Name = "App 2 for User 1", 
            //        AccessKey = "app2key" 
            //    }
            //};
            //appAccounts.ForEach(aa => context.AiApplication.Add(aa));
            //context.SaveChanges();

            //List<Severity> severities = new List<Severity> {
            //    new Severity { Label = "debug" },
            //    new Severity { Label = "info" },
            //    new Severity { Label = "warning" },
            //    new Severity { Label = "error" },
            //    new Severity { Label = "fatal" }
            //};
            //severities.ForEach(s => context.Serverities.Add(s));
            //context.SaveChanges();

            TestDataGenerator gen = new TestDataGenerator(context);
            gen.SmallDataset();
            //List<CustomField> customFields = new List<CustomField> {
            //    new CustomField { EventId = events[0].Id, Name = "IPAddress", Value = "255.255.255.255" },
            //    new CustomField { EventId = events[0].Id, Name = "FileFormat", Value = "CSV" },
            //    new CustomField { EventId = events[0].Id, Name = "ReferringComp", Value = "www.google.com" },
            //    new CustomField { EventId = events[1].Id, Name = "IPAddress", Value = "127.0.0.1" }
            //};
            //customFields.ForEach(cf => context.CustomFields.Add(cf));
            //context.SaveChanges();
        }
    }
}