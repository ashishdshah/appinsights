﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AppInsights.Models
{
    public class Contact
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Your email address")]
        public string Email { get; set; }

        [Display(Name = "Your phone number")]
        public string Phone { get; set; }
        
        [DataType(DataType.MultilineText)]
        [Display(Name = "Reason for contact")]
        public string Reason { get; set; }
    }
}