﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AppInsights.Models
{
    public enum CreateUserStatus
    {
        DUPLICATE_USERNAME,
        DUPLICATE_EMAIL,
        UNSPECIFIED,
        SUCCESS
    }

    public class Customer
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string Name { get; set; }

        [Display(Name = "Customer key")]
        public string Key { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public virtual ICollection<AiApplication> AiApplications { get; set; }

        public static int MinPasswordLength { get { return 6; } }
        
        public bool Validate(AppInsightsContext db) {
            Customer user = db.Customers.FirstOrDefault(u => u.Name == Name && u.Password == Password);
            if (user != null) {
                return true;
            }
            else {
                return false;
            }
        }

        public CreateUserStatus Create(AppInsightsContext db) {
            Customer user = db.Customers.FirstOrDefault(u => u.Name == Name || u.Email == Email);
            if (user == null) {
                db.Customers.Add(this);
                db.SaveChanges();
                return CreateUserStatus.SUCCESS;
            }
            else {
                if (user.Email == Email) return CreateUserStatus.DUPLICATE_EMAIL;
                if (user.Name == Name) return CreateUserStatus.DUPLICATE_USERNAME;
            }
            return CreateUserStatus.UNSPECIFIED;
        }
    }
}