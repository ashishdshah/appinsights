﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AiDataSchema;


namespace AppInsights.Models
{
    public class EventParams
    {
        public string ApplicationKey { get; set; }

        [Display(Name = "From")]
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }

        [Display(Name = "To")]
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }

        [Display(Name = "Aggregation Unit")]
        public string AggregationUnit { get; set; }

        [Display(Name = "Session ID")]
        public string SessionId { get; set; }

        [Display(Name = "Client Direct IP")]
        public string ClientDip { get; set; }

        [Display(Name = "Component")]
        public string Component { get; set; }

        [Display(Name = "Event Type")]
        public string EventType { get; set; }

        [Display(Name = "Severity")]
        public string Severity { get; set; }

        [Display(Name = "Metrics")]
        public ICollection<string> Metrics { get; set; }        
        
        public EventParams() {
            FromDate = DateTime.UtcNow.AddDays(-10);
            ToDate = DateTime.UtcNow;
            AggregationUnit = "day";
            SessionId = "all";
            ClientDip = "all";
            Component = "all";
            EventType = "all";
            Severity = "all";
            Metrics = new List<string>();            
        }

        public TimePeriod TimePeriod
        {
            get
            {
                TimePeriod tp = new TimePeriod();

                switch (AggregationUnit)
                {
                    case "day":
                        tp = TimePeriod.dayOfYear;
                        break;
                    case "week":
                        tp = TimePeriod.week;
                        break;
                    case "month":
                        tp = TimePeriod.month;
                        break;
                    case "year":
                        tp = TimePeriod.year;
                        break;
                };

                return tp;
            }
        }

        public AggregateFilter GetFilter()
        {
            AggregateFilter af = new AggregateFilter();
            af.metrics = Metrics;
            af.fromTime = FromDate;
            af.toTime = ToDate;
            af.aggregationUnit = TimePeriod;
            af.clientDip = ClientDip;
            af.component = Component;
            af.eventType = EventType;
            af.sessionId = SessionId;
            af.severity = Severity;

            return af;
        }

    }
}