﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Models
{
    public class MetricSeriesElement
    {
        public string XValue { get; set; }
        public double YValue { get; set; }
    }
}