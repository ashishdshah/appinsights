﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Models
{
    public class PlotOptions
    {
        public PlotType series { get; set; }
        //public Mode xaxis { get; set; }
        public XAxis xaxis { get; set; }
        public YAxis yaxis { get; set; }
    }

    public class PlotType
    {
        public Show lines { get; set; }
        public Show points { get; set; }
    }

    public class Show
    {
        public bool show { get; set; }
    }


    //public class Mode
    //{
    //    public string mode { get; set; }
    //}

    public class PlotData
    {
        public string label { get; set; }
        public double[][] data { get; set; }
    }

    public class ErrorPlotData : PlotData
    {
        public Points points { get; set; }
    }

    public class Points
    {
        public string errorbars { get; set; }
        public Yerr yerr { get; set; }
    }

    public class Yerr
    {
        public Show show { get; set; }
        public string upperCap { get; set; }
        public string lowerCap { get; set; }
        public int radius { get; set; }
    }

    public class XAxis
    {
        //public string mode { get; set; }
        public double min { get; set; }
        public double max { get; set; }
    }

    public class YAxis
    {
        public double min { get; set; }
        public double max { get; set; }
    }

}