﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AppInsights.Models
{
    public class SupportTicket
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Your email address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Your application key")]
        public string ApplicationKey { get; set; }

        [Required]
        [Display(Name = "Severity of issue")]
        public string Severity {get;set;}

        [Required]
        [Display(Name = "Part of the service affected")]
        public string ServiceArea { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Description of the issue")]
        public string IssueDescription { get;set; }        
    }
}