﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppInsights.Models
{
    public class EventFilter
    {
        public DateTime From { get; set; }
        public string SessionId { get; set; }
        public string RequestId { get; set; }
        public int AppAccountId { get; set; }
        public int SeverityId { get; set; }
        public string ComponentNameKeywords { get; set; }
        public string MessageKeywords { get; set; }
        public string[] Names { get; set; }
        public string[] ValueKeywords { get; set; }
    }
}