﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Helpers;
using AppInsights.Helper;

namespace AppInsights.Models
{
    public class MetricLogParam
    {
        public int ApplicationId { get; set; }
        
        [Display(Name = "Metrics")]
        public ICollection<string> SelectedMetrics { get; set; }

        [Display(Name = "From")]
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }

        [Display(Name = "To")]
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }

        [Display(Name = "Aggregation Unit")]
        public string AggregationUnit { get; set; }

        [Display(Name = "Measure to Show")]
        public string MeasureToShow { get; set; }

        // error specific
        [Display(Name = "Components")]
        public ICollection<string> SelectedComponents { get; set; }

        [Display(Name = "Severity")]
        public ICollection<int> SelectedSeverity { get; set; }        

        public MetricLogParam() {
            AggregationUnit = "day";
            //FromDate = DateTime.UtcNow.AddMonths(-1);
            FromDate = DateTime.UtcNow.AddDays(-10);
            MeasureToShow = "trend";
            SelectedComponents = new List<string>();
            SelectedMetrics = new List<string>();
            SelectedSeverity = new List<int>();            
            ToDate = DateTime.UtcNow;
        }

        public List<PlotData> GetLogData(AppInsightsContext db, out PlotOptions opts) {
            throw new NotImplementedException();

            //TimeSeriesGroup group = TimeSeriesGroupFactory.Create(AggregationUnit, FromDate, ToDate);
            //List<PlotData> plots = new List<PlotData>();
            //List<double> allCounts = new List<double>();
            //foreach (int severityId in SelectedSeverity) {
            //    foreach (string comp in SelectedComponents) {
            //        var vevnts = from e in db.AiEvents
            //                     where e.AiApplicationId == ApplicationId
            //                     && e.Timestamp >= FromDate && e.Timestamp <= ToDate
            //                     && e.SeverityId == severityId
            //                     && e.ComponentName == comp
            //                     orderby e.Timestamp
            //                     select e;
            //        List<AiEvent> evnts = vevnts.ToList();
            //        group.Clear();
            //        foreach (AiEvent evnt in evnts) {
            //            group.AddPoint(evnt.Timestamp, 1);
            //        }
            //        group.GroupingFunction = Utils.Count;
            //        group.Group();
            //        double[][] data = new double[group.XValues.Length][];
            //        for (int i = 0; i < group.XValues.Length; i++) {
            //            data[i] = new double[2];
            //            data[i][0] = Utils.ConvertToUnixTime(group.XValues[i]);
            //            data[i][1] = group.YValues[i];
            //            allCounts.Add(group.YValues[i]);
            //        }
            //        PlotData plot = new PlotData {
            //            data = data,
            //            label = comp + " -- " + db.Serverities.Find(severityId).Label                        
            //        };
            //        plots.Add(plot);
            //    }
            //}
            
            // HACK find the min max of x and y axis
            //double yMin = Math.Floor(allCounts.Min()-1);
            //double yMax = Math.Ceiling(allCounts.Max()+1);
            //double xMin = Utils.ConvertToUnixTime(FromDate.AddDays(-1));
            //double xMax = Utils.ConvertToUnixTime(ToDate.AddDays(1));
            //opts = new PlotOptions {
            //    series = new PlotType {
            //        lines = new Show { show = true },
            //        points = new Show { show = true }
            //    },
            //    xaxis = new XAxis { max = xMax, min = xMin, mode = "time" },
            //    yaxis = new YAxis { max = yMax, min = yMin }
            //};

            //return plots;
        }

        public List<ErrorPlotData> GetMetricErrorPlotData(AppInsightsContext db, out PlotOptions opts) {
            throw new NotImplementedException();

            //TimeSeriesGroup group = TimeSeriesGroupFactory.Create(AggregationUnit, FromDate, ToDate);
            //List<ErrorPlotData> plots = new List<ErrorPlotData>();
            //List<double> allMeans = new List<double>(); // HACK
            //List<double> allSds = new List<double>(); // HACK
            //foreach (string metric in SelectedMetrics) {
            //    var vevnts = from e in db.AiEvents
            //                 where e.AiApplicationId == ApplicationId
            //                 && e.Timestamp >= FromDate && e.Timestamp <= ToDate
            //                 && e.MetricName == metric
            //                 orderby e.Timestamp
            //                 select e;
            //    List<AiEvent> evnts = vevnts.ToList();
            //    group.Clear();
            //    foreach (AiEvent evnt in evnts) {
            //        group.AddPoint(evnt.Timestamp, evnt.MetricValue);
            //    }
            //    group.GroupingFunction = Utils.Average;
            //    group.Group();
            //    double[][] data = new double[group.XValues.Length][];
            //    for (int i = 0; i < group.XValues.Length; i++) {
            //        data[i] = new double[3];
            //        data[i][0] = Utils.ConvertToUnixTime(group.XValues[i]);
            //        data[i][1] = group.YValues[i];
            //        allMeans.Add(group.YValues[i]);
            //    }
            //    group.GroupingFunction = Utils.StdDev;
            //    group.Group();
            //    for (int i = 0; i < group.XValues.Length; i++) {
            //        data[i][2] = group.YValues[i];
            //        allSds.Add(group.YValues[i]);
            //    }
            //    ErrorPlotData plot = new ErrorPlotData {
            //        data = data,
            //        label = metric,
            //        points = new Points {
            //            errorbars = "y",
            //            yerr = new Yerr {
            //                lowerCap = "-",
            //                radius = 5,
            //                show = new Show { show = true },
            //                upperCap = "-"
            //            }
            //        }
            //    };
            //    plots.Add(plot);
            //}
            
            //// HACK find the min max of x and y axis
            //double maxMean = allMeans.Max();
            //double maxSd = allSds.Max();
            //double yMin = Math.Floor(maxMean - maxSd - 1);
            //double yMax = Math.Ceiling(maxMean + maxSd + 1);
            //double xMin = Utils.ConvertToUnixTime(FromDate.AddDays(-1));
            //double xMax = Utils.ConvertToUnixTime(ToDate.AddDays(1));
            //opts = new PlotOptions {
            //    series = new PlotType {
            //        lines = new Show { show = false },
            //        points = new Show { show = true }
            //    },
            //    xaxis = new XAxis { max = xMax, min = xMin, mode = "time" },
            //    yaxis = new YAxis { max = yMax, min = yMin }
            //};
            //return plots;
        }

        public List<PlotData> GetMetricData(AppInsightsContext db, out PlotOptions opts) {
            throw new NotImplementedException();

            //TimeSeriesGroup group = TimeSeriesGroupFactory.Create(AggregationUnit, FromDate, ToDate);
            //List<PlotData> plots = new List<PlotData>();
            //List<double> allMeans = new List<double>(); // HACK
            //foreach (string metric in SelectedMetrics) {
            //    var vevnts = from e in db.AiEvents
            //                 where e.AiApplicationId == ApplicationId
            //                 && e.Timestamp >= FromDate && e.Timestamp <= ToDate
            //                 && e.MetricName == metric
            //                 orderby e.Timestamp
            //                 select e;
            //    List<AiEvent> evnts = vevnts.ToList();
            //    group.Clear();
            //    foreach (AiEvent evnt in evnts) {
            //        group.AddPoint(evnt.Timestamp, evnt.MetricValue);
            //    }
            //    group.GroupingFunction = Utils.Average;
            //    group.Group();
            //    double[][] data = new double[group.XValues.Length][];
            //    for (int i = 0; i < group.XValues.Length; i++) {
            //        data[i] = new double[2];
            //        data[i][0] = Utils.ConvertToUnixTime(group.XValues[i]);
            //        data[i][1] = group.YValues[i];
            //        allMeans.Add(group.YValues[i]);
            //    }
            //    PlotData plot = new PlotData {
            //        data = data,
            //        label = metric                   
            //    };
            //    plots.Add(plot);
            //}

            // HACK find the min max of x and y axis
            //double yMin = Math.Floor(allMeans.Min() - 1);
            //double yMax = Math.Ceiling(allMeans.Max() + 1);
            //double xMin = Utils.ConvertToUnixTime(FromDate.AddDays(-1));
            //double xMax = Utils.ConvertToUnixTime(ToDate.AddDays(1));
            //opts = new PlotOptions {
            //    series = new PlotType {
            //        lines = new Show { show = true },
            //        points = new Show { show = true }
            //    },
            //    xaxis = new XAxis { max = xMax, min = xMin, mode = "time" },
            //    yaxis = new YAxis { max = yMax, min = yMin }
            //};

            //return plots;
        }
    }
}
