﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.IO;

namespace DotNetClient
{
    public class AiClient
    {
        private const int MAX_QUEUE_LENGTH = 200;
        private const int MAX_INTERVAL_MSECS = 2 * 60 * 1000;
        private const string DATACHANNEL_URL = "http://www.streamviolet.com/api/DataStream";

        private Batch _batch;
        private Timer _timer;
        private object _locker;
        
        public AiClient(string applicationKey) {
            ApplicationKey = applicationKey;
            _batch = new Batch(ApplicationKey);
            _locker = new object();
            _timer = new Timer(o => this.PostEventStream());
            _timer.Change(MAX_INTERVAL_MSECS, MAX_INTERVAL_MSECS);
        }

        public string ApplicationKey { get; set; }

        public void WriteDataStream(Event evnt) {
            lock (_locker) {
                _batch.events.Add(evnt);
                if (_batch.numberOfEvents >= MAX_QUEUE_LENGTH) {                    
                    Thread post = new Thread(this.PostEventStream);
                    post.Start();
                }
            }
        }

        public void Flush() {
            PostEventStream();
        }

        // TODO: There is a race condition which results in very aggressive server calling.
        // The queue length reaches max -> POST starts -> before the _batch object is locked, items continue to be added to the queue -> another POST starts
        // The queue length reaches max -> POST starts -> the timer expires and another POST starts
        private void PostEventStream() {
            string batch = "";
            int numEvents = 0;                            
            lock (_locker) {
                if (_batch.numberOfEvents == 0) {
                    return;
                }
                numEvents = _batch.numberOfEvents;                
                batch = _batch.ToJsonString();
                _batch.Clear();
            }
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(DATACHANNEL_URL);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";            

            byte[] bytedata = Encoding.UTF8.GetBytes("eventStream=" + batch);
            webRequest.ContentLength = bytedata.Length;

            Stream requestStream = webRequest.GetRequestStream();
            requestStream.Write(bytedata, 0, bytedata.Length);
            requestStream.Close();

            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            // log the http status code 
            webResponse.Close();
        }
    }
}
