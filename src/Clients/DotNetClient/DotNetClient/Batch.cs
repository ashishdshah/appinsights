﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DotNetClient
{
    public class Metric
    {
        public string name { get; set; }
        public object value { get; set; }

        public string ToJsonString() {
            return JsonConverter.Serialize<Metric>(this);
        }
    }

    public class ObjectId
    {
        public int _increment;
        public int _machine;
        public int _pid;
        public int _timestamp;

        public ObjectId() {
            _increment = _machine = _pid = _timestamp = 0;
        }
    }

    public class Event
    {
        public ObjectId _id { get; private set; }
        public DateTime timeStamp { get; set; }
        public string sessionId { get; set; }
        public string clientDip { get; set; }
        public string componentName { get; set; }

        public string eventType { get; set; }
        public string severity { get; set; }
        public string message { get; set; }
        public ICollection<Metric> metrics { get; set; }

        public Event() {
            _id = new ObjectId();
            timeStamp = DateTime.Now;
            componentName = System.Reflection.Assembly.GetExecutingAssembly().FullName;
            eventType = ".NET client";
        }

        public string ToJsonString() {
            return JsonConverter.Serialize<Event>(this);
        }
    }

    [DataContract]
    public class Batch
    {
        [DataMember]
        public string applicationKey { get; set; }

        [DataMember]
        public int numberOfEvents {
            get { return events.Count; }
            private set { }
        }

        [DataMember]
        public ICollection<Event> events { get; set; }

        public Batch(string applicationKey) {
            this.applicationKey = applicationKey;
            events = new List<Event>();            
        }

        public void Clear() {
            events.Clear();
        }

        public string ToJsonString() {
            return JsonConverter.Serialize<Batch>(this);
        }


    }
}
