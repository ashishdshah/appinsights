<?php

	// needs to be initialized and you need at least one per session. you can have multiple instances per session, but one per application will pose a problem in handling multiple sessions in one instance
	class AiPhpDataChannelClient
	{
		protected $_applicationKey;
		protected $_sessionId;
		protected $_url;
		protected $_clientIp;
		protected $_percent_to_send;
		
		// assume that URL, application key and session id will always be provided
		public function AiPhpDataChannelClient($url, $applicationKey, $percent_to_send)
		{
			// session_start() either starts a new session or resumes the current one
			session_start();
			$this->_applicationKey = (string)$applicationKey;
			$this->_sessionId = session_id();		// auto session id
			$this->_url = (string)$url;
			$this->_clientIp = getHostByName(php_uname('n')); // '12.34.56.78';
			$this->_percent_to_send = $percent_to_send;
		}

		private function pushAiStream($arr)
		{
			$json = 'eventStream=' . json_encode($arr);
			$params = array('http' => array(
					  'method' => 'POST',
					  'header' => 'Content-Type: application/x-www-form-urlencoded',
					  'content' => $json
					));

		    $ctx = stream_context_create($params);
			try {
				$fp = @fopen($this->_url, 'rb', false, $ctx);
				$response = @stream_get_contents($fp);
				@fclose($fp);
			}
			catch(Exception $e) {
				@fclose($fp);
			}
			$response = true;
		    return $response;
		}
		
		// make metrics reuired and the others optional. metrics should be a 2d associative array
		public function writeAiStream($metrics, $componentName = "AiDataChannelClient.php", $eventType = "PhpClient", $message = "", $severity = "")
		{
			$resp = '';
			
			// client ip, timestamp, session id, application key to be added by default
			if (rand(1,100) < $this->_percent_to_send) {
				$timeStamp = "/Date('" . date("d-m-Y h:i:s a") . "')/";
				
				$arr = array("applicationKey" => $this->_applicationKey, "events" => array(array("sessionId" => $this->_sessionId, "timestamp" => $timeStamp, "clientDip" => $this->_clientIp, "componentName" => $componentName, "eventType" => $eventType, "message" => $message, "severity" => $severity, "metrics" => $metrics)));
				$resp = $this->pushAiStream($arr);
			}
			return $resp;
		}
	}
?>
