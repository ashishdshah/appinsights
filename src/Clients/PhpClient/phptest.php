<?php 
	require "AiDataChannelClient.php";

	// Change the URL in parameter 1 below. Also change the application key in parameter 2. The third parameter is the percent of entriess you want logged.
	$aiDataClient = new AiPhpDataChannelClient('http://www.streamviolet.com/api', '<your application key here>', 100);
	$aiDataClient->writeAiStream(array(array("name" => "cpu", "value" => 1), array("name" => "geoRegion", "value" => "value2")));
	$aiDataClient->writeAiStream(array(array("name" => "cpu", "value" => 2), array("name" => "geoRegion", "value" => "value3")), "MyComponentName");
	$aiDataClient->writeAiStream(array(array("name" => "cpu", "value" => 4), array("name" => "geoRegion", "value" => "value3")), "MyComponentName");
	$aiDataClient->writeAiStream(array(array("name" => "cpu", "value" => 2), array("name" => "geoRegion", "value" => "value3")), "MyComponentName");
	$aiDataClient->writeAiStream(array(array("name" => "cpu", "value" => 6), array("name" => "geoRegion", "value" => "value3")), "MyComponentName");
	
	echo "<h1>Events posted. Check the database to see if they made it</h1>";

?>