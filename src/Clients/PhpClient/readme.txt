phptest.php: test file
AiDataChannelClient.php: file to be included

function signature:
public function writeAiStream($metrics, $componentName = "AiDataChannelClient.php", $eventType = "PhpClient", $message = "", $severity = "")

sample:
0. I will create you an application once i host this and give you the app key or give you the URL to get your own app key

1. require AiDataChannelClient.php in your php file

2. create the object
	$aiDataClient = new AiPhpDataChannelClient('http://localhost:51420/DataStream', 'appkey');
3. Send the event
	$aiDataClient->writeAiStream(array(array("name" => "name1", "value" => "value1"), array("name" => "name2", "value" => "value2")));
4. send another event
	$aiDataClient->writeAiStream(array(array("name" => "name1", "value" => "value11"), array("name" => "name2", "value2" => "value2")), "MyComponentName");